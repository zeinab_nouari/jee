<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Audits</title>

</head> 
<body>
<%@ include file="css.jsp"%>

<header>
	<jsp:include page="/WEB-INF/header.jsp"/>
</header>
<div class="section"><center><h1><t>Vos audits </t> </h1> </center> </div>



<div class=main>

<c:if test="${ ! empty liste   }">

<table class="styled-table">
<thead>
        <tr>
            <th>Nom</th>
            <th>Description</th>
            <th>Etat</th>
            <th>Details</th>
        </tr>
    </thead>
    
    <tbody>
        

<c:forEach items="${liste}" var="liste">
<tr>
            <td><c:out value= " ${liste.nom }" /></td>
            <td><c:out value= "${liste.description}" /></td>
            <td> <c:out value= " ${liste.etat }" />
            <c:if test="${ liste.etat == 'cree'}">

        <br> <a href="#"> Planifier cet audit   </a>
       
</c:if>



<c:if test="${ liste.etat == 'termine'}">
<br> <a href="#"> Consulter les r�sultats </a> </c:if>
          
          



<td>

<a href="/projetS8-callisto/DetailsAudit?id=${liste.idAudit }">D�tails de l'audit</a>
 </td> 
        </tbody>




</c:forEach> 
</table>
  </c:if>
<c:if test="${ empty liste   }">
<c:out value="Aucun audit � consulter" />  </c:if>


</div> 


</body>
</html>