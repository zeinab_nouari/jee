<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Accueil</title>
</head>
<body>
	<jsp:include page="/WEB-INF/css.jsp"/>
	
<header>
	<jsp:include page="/WEB-INF/header.jsp"/>
</header>
	
<div class="main">

<c:if test="${ !empty errorMessage }">
<div class="error-message">
	<i class="fa exclamation-triangle"></i>
	<c:out value="Erreur : ${ errorMessage }"/>
</div>
</c:if>

<c:if test="${ !empty message }">
<div class="valid-message">
	<i class="fa check-circle"></i>
	<c:out value="${ message }" />
</div>
</c:if>	
	
	<div id="button">
		<a href="/projetS8-callisto/listeAudits"><input type="submit" value="Liste des audits"/></a>
		<a href=""><input type="submit" value="R�sultat brut"/></a>
		<a href=""><input type="submit" value="Audit pour cr�ation"/></a>
		<a href=""><input type="submit" value="Audit pour mod�le"/></a>
		<c:if test="${ (!empty sessionScope.utilisateurConnecte) }">
		<c:if test="${sessionScope.utilisateurConnecte.userId==0}">
		<a href="roles"><input type="submit" value="Changer les r�les des utilisateurs"></a>
		</c:if>
		
		</c:if>
	</div>
	</div>
</body>
</html>