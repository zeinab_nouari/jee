<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
</head>
<body>
	
	<div id="above-header">
		<c:if test="${!empty sessionScope.utilisateurConnecte }">
		<div class="connected-user">
		<i class="fa user"></i>
		<c:out value="${ sessionScope.utilisateurConnecte.getNom() } ${ sessionScope.utilisateurConnecte.getPrenom() }" />
		<a href="deconnexion"><i class="fa sign-out"></i>D�connexion</a>
		</div>
		</c:if>
		<c:if test="${empty sessionScope.utilisateurConnecte }">
		<ul><li><a href="connexion"><i class="fa connexion"></i>Se connecter</a></li></ul>
		</c:if>
		<%-- <c:out value=" Pr�nom NOM "></c:out>--%>
	</div>
	
	<img id="baniere" src="img/baniere.jpg" >
	<img id="logo" src="img/logoEseo.png" >
	
	<div  id="bar-nav">
	<ul class="bar-nav-element">
		<li><a href="./home"><i class="fa retour-accueil"></i>Accueil</a></li>
	</ul>
	</div>
	<script type="text/javascript">
	window.onscroll = function() {setStatus()};
	var bar = document.getElementById("bar-nav");
	var sticky = bar.offsetTop;
	function setStatus() {
	if (window.pageYOffset <= sticky) {
	 bar.classList.remove("sticky")
	} else {
	 bar.classList.add("sticky");
	}} 
	</script>
</body>
</html>