<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Roles des utilisateurs</title>
</head>
<body>
<h1>Changer le role des utilisateurs</h1>
<c:if test="${ !empty errorMessage }">
	<c:out value="Erreur : ${ errorMessage }"/>
</c:if>
<div class="form-change-role">
<form method="post" id="formRole" accept-charset="UTF-8">
<table>
<tr>
	<th><label for="listeUtilisateurs">Utilisateur :</label></th>
	<th><select class="liste" id="listeUtilisateurs" name="listeUtilisateurs">
		<c:forEach items="${ listeUtilisateurs }" var="utilisateur">
			<option value="${ utilisateur.getUserId() }">
				<c:out value="${ utilisateur.getNom() } ${ utilisateur.getPrenom() }"/>
			</option>
		</c:forEach>
	</select></th>
</tr>
<tr>
	<th><label for="roles">Role :</label></th>
	<th><select id="roles" name="roles">
		<option value="eleve">El�ve</option>
		<option value="interne">Interne</option>
		<option value="responsable-ue">Responsable d'UE</option>
		<option value="responsable-option">Responsable d'option</option>
		<option value="responsable-matiere">Responsable mati�re</option>
		<option value="encadrant-matiere">Encadrant mati�re</option>
	</select></th>
</tr>
<tr>
	<th><label for="listeUe">UE :</label></th>
	<th><select class="liste" id="listeUe" name="listeUe">
		<c:forEach items="${ listeUe }" var="ue">
			<option value="${ ue.getIdUe() }"><c:out value="${ ue.getNomUe() }"/></option>
		</c:forEach>
	</select></th>
</tr>
<tr>
	<th><label for="listeOptions">Options :</label></th>
	<th><select class="liste" id="listeOptions" name="listeOptions">
		<c:forEach items="${ listeOptions }" var="option">
			<option value="${ option.getIdOption() }"><c:out value="${ option.getNomOption() }"/></option>
		</c:forEach>
	</select></th>
</tr>
<tr>
	<th><label for="listeMatieres">Mati�res :</label></th>
	<th><select class="liste" id="listeMatieres" name="listeMatieres">
		<c:forEach items="${ listeMatieres }" var="matiere">
			<option value="${ matiere.getIdMatiere() }"><c:out value="${ matiere.getNomMatiere() }"/></option>
		</c:forEach>
	</select></th>
</tr>
<tr>
<th colspan="2"><input type="submit" name="changeRole" value="Changer le role"/></th>
</tr>
</table>
</form>
</div>

<c:if test="${ !empty message }"><c:out value="${ message }"/> </c:if>

</body>
</html>