<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" crossorigin="anonymous">
<style type="text/css">

body{
	background-color : rgba(0,10,0,0.05);
}

a {
	color: #5A13A9;
	text-decoration:none;
}

 a:hover{
	color: red;
}

t{
color: #5A13A9;
text-decoration:none;
}

/*form  {
   position:absolute;
   left:50%;
   width:300px; 
   margin-left:-150px; 
}*/

input[type=text], input[type=password]{
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

#above-header{
	background-color: white;
	color: black;
	font-size:20px;	
	text-align: right;	
	width:100%;
	vertical-align: middle;
}

#above-header * {
	margin: 0 5px 0 5px;
	/*float: right;*/
	height: 25px;
}

#above-header ul {
	list-style: none;
}

img {
    max-width: 100%;
    height: auto;
}

#imgprofildefaut{	
	width : 25px;
	height : 25px;
}

#baniere{
	width: 100%;
	z-index : 2;
	position:relative;
}

#logo{
	height : 90px;
	width : 200px;
	position : absolute;
	z-index : 4;
	top:40px;
	left:40%;
}

#button{
	position: absolute; 
	top: 150px;
	height: auto;
	
}

input[type=submit] {
	text-align: left;
	font-size: 100%;
    background-color: #E3E3E3;
    color: #5A13A9;
    padding: 14px 20px;
    margin: 2px 0;
    border: none;
    border-radius: 5px;
    cursor: pointer;
    width: 30% !important;
    text-align: center;
}



input[type=submit]:hover {
    background-color: #E3E3E3;
    color: red;
}

.retour-accueil:before {
	content: "\f015";
}

.edit-user:before {
	content: "\f4ff";
}

.check-circle:before{
	content: "\f058";
}

.exclamation-triangle:before{
	content: "\f071";
}

.connexion:before{
	content: "\f2c2";
}

.fa {
	font-family: "Font Awesome 5 Free";
    vertical-align: middle;
    color: inherit;
    margin-right: 5px;
}

.sign-out:before{
	content: "\f2f5";
}

.user:before{
	font-family: "Font Awesome 5 Free";
    padding: 10px;
    vertical-align: middle;
	content: "\f007";
}

h1 {
	position: relative;
	margin-left:10px;
	margin-right: auto;
	margin-top:5px;
	margin-bottom:5px;
}

#bar-nav {
	background-color:#5a4d9b;
	color:#ffffff;
	height:30px;
	width:100%;
	overflow: hidden;
	z-index: 100;
}

#bar-nav * {
	text-decoration:none;
	color:#ffffff;
}

#bar-nav ul.bar-nav-element {
	margin: 0 0 0 10px;
	list-style: none;
	padding: 0px;
}

#bar-nav ul.bar-nav-element li{
	max-width: max-content;
	height: 30px;
	display: table-cell;
	vertical-align: middle;
}

#bar-nav ul.bar-nav-element li * {
	margin: 0 5px 0 5px;
}

#bar-nav ul.bar-nav-element li:hover{
	background-color: #3b3364;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

.vert-centered{
	margin: 0;
	position: relative;
	top: 50%;
	-ms-transform: translateY(-50%);
	transform: translateY(-50%);
}

header {
	width:100%;
	position: relative;
	left:0px;
	top:0px;
	z-index: 5;
}

.main{
	position: relative;
	top:30%;
	width:100%;
	margin: 20px 0 20px 0;
}

.section{
	position:relative;
	margin:3%;
	border:1px solid #c8c8c8;
	box-shadow:0 0 8px #dedede;
}

.cadre {
	position:relative;
	margin:3%;
 	border-radius: 10px;
  	background-color: #f2f2f2;
  	
  	}
  	
.content{ 
	margin:10px;
}

.submit-table {
	width:80%;
	margin-left: auto;
	margin-right: auto;
}

.submit-table th.right-align{
	text-align: right;
	width:20%;
}

.submit-table th.left-align{
	text-align: left;
	width:30%;
}

.submit-table th *{
	width:50%;
}

.submit-table th input[type=radio]{
	width:10px;
}


*{
	margin:0;
	color: #403f3a;
	font-family: Open Sans,sans-serif;
}

.submit-table select,input,radio{
	color:#5A13A9;
}

.submit-table select:hover,input:hover{
	color:red;
}

.error-message{
	position:absolute;
	top:30px;
	height: 20px;
	min-width:30%;
	background-color: #cf272744;
	right: 30px;
	border-radius: 4px;
	text-align: center;
	box-shadow:0 0 10px #dedede;
}

.valid-message{
	position:absolute;
	top:30px;
	height: 20px;
	min-width:30%;
	background-color: #02ab0244;
	right: 30px;
	border-radius: 4px;
	text-align: center;
	box-shadow:0 0 10px #dedede;
}

table { 
	width: 750px; 
	border-collapse: collapse; 
	margin:50px auto;
	position: relative;
	}

/* Zebra striping */
tr:nth-of-type(odd) { 
	background: #eee; 
	}

th { 
	background: #5a4d9b; 
	color: white; 
	font-weight: bold; 
	}

td, th { 
	padding: 10px; 
	border: 1px solid #ccc; 
	text-align: left; 
	font-size: 18px;
	}




</style>