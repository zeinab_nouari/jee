package fr.eseo.ld.callisto.projetgl.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eseo.ld.callisto.projetgl.beans.Audit;
import fr.eseo.ld.callisto.projetgl.dao.DaoException;
import fr.eseo.ld.callisto.projetgl.dao.DaoFactory;
import fr.eseo.ld.callisto.projetgl.dao.UtilisateurDao;

/**
 * Servlet implementation class DetailsAudit
 */
@WebServlet("/DetailsAudit")
public class DetailsAudit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetailsAudit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		UtilisateurDao usersDB = null ;
		if (request.getParameter("id") != null) {
			int idAudit = Integer.parseInt(request.getParameter("id"));
		try {
			usersDB = DaoFactory.getImpl2();
			//usersDB.addPersonne(chefPrincipal);
			
		} catch (DaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			Audit audit = usersDB.getAuditById(idAudit) ;
			request.setAttribute("audit", audit);
		} catch (DaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		this.getServletContext().getRequestDispatcher("/WEB-INF/detailAudit.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
