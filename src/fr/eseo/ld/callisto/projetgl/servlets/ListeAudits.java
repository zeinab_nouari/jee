package fr.eseo.ld.callisto.projetgl.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eseo.ld.callisto.projetgl.beans.Audit;
import fr.eseo.ld.callisto.projetgl.beans.BeanException;
import fr.eseo.ld.callisto.projetgl.beans.Eleve;
import fr.eseo.ld.callisto.projetgl.beans.Encadrant;
import fr.eseo.ld.callisto.projetgl.beans.Responsable;
import fr.eseo.ld.callisto.projetgl.beans.Utilisateur;
import fr.eseo.ld.callisto.projetgl.dao.DaoException;
import fr.eseo.ld.callisto.projetgl.dao.DaoFactory;
import fr.eseo.ld.callisto.projetgl.dao.UserDao;
import fr.eseo.ld.callisto.projetgl.dao.UtilisateurDao;

/**
 * Servlet implementation class ListeAudits
 */
@WebServlet("/ListeAudits")
public class ListeAudits extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListeAudits() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String roleChoisi = request.getParameter("roleChoisi");
		Object role = null ;
		UtilisateurDao usersDB = null ;
		try {
			usersDB = DaoFactory.getImpl2();
			//usersDB.addPersonne(chefPrincipal);
			
		} catch (DaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Utilisateur utilisateur = null ;
		Encadrant encadrant = null;
		Eleve eleve = null ;
		Responsable responsable = null ;
		List <Audit> liste = null ;
		
		
		try {
			utilisateur = (Utilisateur) session.getAttribute("utilisateur");
			
			//if (usersDB.estResponsable(utilisateur) && usersDB.estEncadrant(utilisateur) && roleChoisi == null) {
			//	role = "doubleRole";
			//	request.setAttribute("role", role);
				
			//	response.sendRedirect("ChoixRole");
				//this.getServletContext().getRequestDispatcher("/WEB-INF/ListeAudits.jsp").forward(request, response);}
				
			
			if((usersDB.estEncadrant(utilisateur))) {
			
			encadrant = usersDB.getEncadrant(utilisateur);
			role = "encadrant";
			try {
				liste = usersDB.getListeAuditsByEncadrant(encadrant) ;
				request.setAttribute("liste",liste);
				request.setAttribute("role",role);
			} catch (DaoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.getServletContext().getRequestDispatcher("/WEB-INF/ListeAudits.jsp").forward(request, response);}
			
			else if (usersDB.estEleve(utilisateur)) {
				role = "eleve";
				eleve = usersDB.getEleve(utilisateur);
				try {
					liste = usersDB.getListeAuditsByEleve(eleve) ;
					request.setAttribute("liste",liste);
					request.setAttribute("role",role);
				} catch (DaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				this.getServletContext().getRequestDispatcher("/WEB-INF/ListeAudits.jsp").forward(request, response);
			}
			
			else if ((usersDB.estResponsable(utilisateur)) || (roleChoisi == "responsableMatiere")) {
				role = "responsableMatiere";
				responsable = usersDB.getResponsable(utilisateur);
				try {
					liste = usersDB.getListeAuditsByRespMatiere(responsable) ;
					request.setAttribute("liste",liste);
					request.setAttribute("role",role);
				} catch (DaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				this.getServletContext().getRequestDispatcher("/WEB-INF/ListeAudits.jsp").forward(request, response);
			}
			
					
			
		} catch (BeanException | DaoException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		String roleChoisi = request.getParameter("roleChoisi");
		if (roleChoisi != null) {
		response.sendRedirect("/projetS8-callisto/listeAudits?roleChoisi=responsable");  
		
	}

} }
