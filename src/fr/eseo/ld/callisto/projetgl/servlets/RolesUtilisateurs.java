package fr.eseo.ld.callisto.projetgl.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eseo.ld.callisto.projetgl.beans.BeanException;
import fr.eseo.ld.callisto.projetgl.beans.Matiere;
import fr.eseo.ld.callisto.projetgl.beans.Option;
import fr.eseo.ld.callisto.projetgl.beans.Ue;
import fr.eseo.ld.callisto.projetgl.beans.Utilisateur;
import fr.eseo.ld.callisto.projetgl.beans.formulaires.FormulaireChangerRoleUtilisateur;
import fr.eseo.ld.callisto.projetgl.dao.DaoException;
import fr.eseo.ld.callisto.projetgl.dao.DaoFactory;

@WebServlet("/RolesUtilisateurs")
public class RolesUtilisateurs extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private List<Utilisateur> listeUtilisateurs;
	private String errorMessage;
	private List<Ue> listeUe;
	private List<Option> listeOptions;
	private List<Matiere> listeMatieres;

	private String message;
       
    public RolesUtilisateurs() {
        super();
    }
    
    public void initListes() {
    	try {
			listeUtilisateurs = DaoFactory.getImpl().getListeUtilisateurs();
			listeOptions = DaoFactory.getImpl().getListeOptions();
			listeUe = DaoFactory.getImpl().getListeUe();
			listeMatieres = DaoFactory.getImpl().getListeMatieres();
		} catch (DaoException e) {
			errorMessage = e.getMessage();
		}
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		//TODO A enlever quand il y aura la page de connexion
		session.setAttribute("user", new Utilisateur(0,"admin","","","","admin",false));
		//
		if(request.getSession().getAttribute("user")!=null) {
    		Utilisateur connectUser = (Utilisateur) request.getSession().getAttribute("user");
    		if(connectUser.getUserId()==0 && connectUser.getNom().equals("admin")) {
    			initListes();
    			request.setAttribute("listeUtilisateurs", listeUtilisateurs);
				request.setAttribute("listeUe", listeUe);
				request.setAttribute("listeOptions", listeOptions);
				request.setAttribute("listeMatieres", listeMatieres);
				request.setAttribute("errorMessage", errorMessage);
				request.setAttribute("message", message);
				this.getServletContext().getRequestDispatcher("/WEB-INF/rolesUtilisateurs.jsp").forward(request, response);
				return;
    		}
		}
		this.getServletContext().getRequestDispatcher("/WEB-INF/unauthorized.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		if(request.getParameter("changeRole")!=null) {
			FormulaireChangerRoleUtilisateur formRole = new FormulaireChangerRoleUtilisateur(request);
			try {
				formRole.valid();
				message = formRole.getMessage();
				errorMessage = null;
			} catch (BeanException | DaoException e) {
				errorMessage = e.getMessage();
			}
		}
		doGet(request, response);
	}

}
