package fr.eseo.ld.callisto.projetgl.utils;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashClass {
	
	public static byte[] getSHA(String input) throws NoSuchAlgorithmException 
    {   
        MessageDigest md = MessageDigest.getInstance("SHA-256");  
        return md.digest(input.getBytes(StandardCharsets.UTF_8));  
    } 
    
    public static String toHexString(byte[] hash) 
    { 
        BigInteger number = new BigInteger(1, hash);  
        StringBuilder hexString = new StringBuilder(number.toString(16));  
        while (hexString.length() < 32)  
        {  
            hexString.insert(0, '0');  
        }  
        return hexString.toString();  
    } 
    
    public static String hashIt(String pwd, String smthMore, int nRepeat) throws NoSuchAlgorithmException {
    	String result = HashClass.toHexString(HashClass.getSHA(pwd+smthMore));
    	for(int i=0;i<nRepeat;i++) {
    		result = HashClass.toHexString(HashClass.getSHA(result+i));
    	}
    	return result;
    }

}
