package fr.eseo.ld.callisto.projetgl.beans;

public class Encadrant extends Utilisateur{
    private int idEncadrant;

    public Encadrant(int userId, String nom, String prenom, String adresseMail, int idEncadrant) throws BeanException {
        super(userId, nom, prenom, adresseMail);
        this.idEncadrant = idEncadrant;
    }

    public Encadrant(Utilisateur utilisateur) throws BeanException {
        super(utilisateur.getUserId(), utilisateur.getNom(), utilisateur.getPrenom(), utilisateur.getAdresseMail());
    }

	public int getIdEncadrant() {
        return idEncadrant;
    }

    public void setIdEncadrant(int idEncadrant) {
        this.idEncadrant = idEncadrant;
    }
}
