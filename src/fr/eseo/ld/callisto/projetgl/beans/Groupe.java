package fr.eseo.ld.callisto.projetgl.beans;

public class Groupe {
    private int idGroupe;
    private String description;
    private int taille;

    public Groupe() {
    }

    public Groupe(int idGroupe, String description, int taille) {
        this.idGroupe = idGroupe;
        this.description = description;
        this.taille = taille;
    }

    public int getIdGroupe() {
        return idGroupe;
    }

    public void setIdGroupe(int idGroupe) {
        this.idGroupe = idGroupe;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTaille() {
        return taille;
    }

    public void setTaille(int taille) {
        this.taille = taille;
    }
}
