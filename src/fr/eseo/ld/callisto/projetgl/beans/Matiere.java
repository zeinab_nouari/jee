package fr.eseo.ld.callisto.projetgl.beans;

public class Matiere {
    private int idMatiere;
    private String nomMatiere;
    private int idRespMatiere;
	private Utilisateur responsableMatiere;

    public Matiere() {
    }

    public Matiere(int idMatiere, String nomMatiere, int idRespMatiere) {
        this.idMatiere = idMatiere;
        this.nomMatiere = nomMatiere;
        this.idRespMatiere = idRespMatiere;
    }

    public Matiere(int idMatiere, String nomMatiere, Utilisateur responsable) {
    	this.idMatiere = idMatiere;
        this.nomMatiere = nomMatiere;
        this.responsableMatiere = responsable;
	}

	public int getIdMatiere() {
        return idMatiere;
    }

    public void setIdMatiere(int idMatiere) {
        this.idMatiere = idMatiere;
    }

    public String getNomMatiere() {
        return nomMatiere;
    }

    public void setNomMatiere(String nomMatiere) {
        this.nomMatiere = nomMatiere;
    }

    public int getIdRespMatiere() {
        return idRespMatiere;
    }

    public void setIdRespMatiere(int idRespMatiere) {
        this.idRespMatiere = idRespMatiere;
    }

	public Utilisateur getResponsableMatiere() {
		return responsableMatiere;
	}

	public void setResponsableMatiere(Utilisateur responsableMatiere) {
		this.responsableMatiere = responsableMatiere;
	}
    
}
