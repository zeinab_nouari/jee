package fr.eseo.ld.callisto.projetgl.beans;

import java.util.Date;
import java.util.List;

public class Audit {
    private String description;
    private String nom;
    private int idAudit;
    private Date dateCreation;
    private Date dateAudit;
    private int idEncadrant;
    private int idMatiere;
    private String etat ;
    private List <Eleve> listeEleves ;
    private Encadrant encadrant ;
    private Responsable responsableMatiere ;
    private Groupe groupe ;
    private Matiere matiere ;
    
  //  private enum etat{cree, planifie, enCours, termine, archive};

    public Audit() {
    }

    public Audit(int idAudit, String nom, String description, java.sql.Date dateCreation, java.sql.Date dateAudit,String etat) throws BeanException {
        this.setIdAudit(idAudit);
        this.setNom(nom);
        this.setDescription(description);
        this.setDateCreation(dateCreation);
        this.setDateAudit(dateAudit);
        this.idEncadrant = idEncadrant;
        this.idMatiere = idMatiere;
        this.etat = etat ;
    }

  //  public Audit(int int1, String string, String string2, java.sql.Date date, java.sql.Date date2, String string3) {
		// TODO Auto-generated constructor stub
	//}

	public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) throws BeanException{
        if(nom.length() >= 15){
            throw new BeanException("");
        }
        this.nom = nom;
    }

    public int getIdAudit() {
        return idAudit;
    }

    public void setIdAudit(int idAudit) {
        this.idAudit = idAudit;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateAudit() {
        return dateAudit;
    }

    public void setDateAudit(Date dateAudit) {
        this.dateAudit = dateAudit;
    }
    
    

    public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}
	
	

	public List<Eleve> getListeEleves() {
		return listeEleves;
	}

	public void setListeEleves(List<Eleve> listeEleves) {
		this.listeEleves = listeEleves;
	}

	public Encadrant getEncadrant() {
		return encadrant;
	}

	public void setEncadrant(Encadrant encadrant) {
		this.encadrant = encadrant;
	}

	public Responsable getResponsableMatiere() {
		return responsableMatiere;
	}

	public void setResponsableMatiere(Responsable responsableMatiere) {
		this.responsableMatiere = responsableMatiere;
	}

	public Groupe getGroupe() {
		return groupe;
	}

	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}

	public Matiere getMatiere() {
		return matiere;
	}

	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	@Override
    public String toString() {
        return "Audit{" +
                "description='" + description + '\'' +
                ", nom='" + nom + '\'' +
                ", idAudit=" + idAudit +
                ", dateCreation=" + dateCreation +
                ", dateAudit=" + dateAudit +
                ", idEncadrant=" + idEncadrant +
                ", idMatiere=" + idMatiere +
                '}';
    }

}
