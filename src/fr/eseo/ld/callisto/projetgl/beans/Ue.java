package fr.eseo.ld.callisto.projetgl.beans;

public class Ue {
    private int idUe;
    private String nomUe;
    private int idResponsableUe;
	private Utilisateur responsableUe;

    public Ue() {
    }

    public Ue(int idUe, String nomUe, int responsableUe) {
        this.idUe = idUe;
        this.nomUe = nomUe;
        this.idResponsableUe = responsableUe;
    }

    public Ue(int idUe, String nomUe, Utilisateur responsable) {
    	this.idUe = idUe;
    	this.nomUe = nomUe;
    	this.responsableUe = responsable;
	}

	public int getIdUe() {
        return idUe;
    }

    public void setIdUe(int idUe) {
        this.idUe = idUe;
    }

    public String getNomUe() {
        return nomUe;
    }

    public void setNomUe(String nomUe) {
        this.nomUe = nomUe;
    }

	public int getIdResponsableUe() {
		return idResponsableUe;
	}

	public void setIdResponsableUe(int idResponsableUe) {
		this.idResponsableUe = idResponsableUe;
	}
	
	public Utilisateur getResponsableUe() {
		return responsableUe;
	}

	public void setResponsableUe(Utilisateur responsableUe) {
		this.responsableUe = responsableUe;
	}
    
    
}
