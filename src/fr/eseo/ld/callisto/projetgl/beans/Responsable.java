package fr.eseo.ld.callisto.projetgl.beans;


public class Responsable extends Utilisateur{
    private int idResponsable;

    public Responsable(int id, String nom, String prenom, String adresseMail, int idResponsable) throws BeanException {
        super(id, nom, prenom, adresseMail);
        this.idResponsable = idResponsable;
    }

    public Responsable(Utilisateur utilisateur, int int1) {
		// TODO Auto-generated constructor stub
	}

	public Responsable(Utilisateur utilisateur) throws BeanException {
		super(utilisateur.getUserId(), utilisateur.getNom(), utilisateur.getPrenom(), utilisateur.getAdresseMail());
	}

	public int getIdResponsable() {
        return idResponsable;
    }

    public void setIdResponsable(int idResponsable) {
        this.idResponsable = idResponsable;
    }

}
