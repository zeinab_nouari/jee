package fr.eseo.ld.callisto.projetgl.beans;

public class Question {
    private int idQuestion;
    private String question;
    private enum type{texte, checkbox};
    private int idAudit;
    private int encadrantReponse;

    public Question() {
    }

    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getIdAudit() {
        return idAudit;
    }

    public void setIdAudit(int idAudit) {
        this.idAudit = idAudit;
    }

    public int getEncadrantReponse() {
        return encadrantReponse;
    }

    public void setEncadrantReponse(int encadrantReponse) {
        this.encadrantReponse = encadrantReponse;
    }
}
