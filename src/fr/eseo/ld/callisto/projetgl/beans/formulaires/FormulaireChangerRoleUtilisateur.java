package fr.eseo.ld.callisto.projetgl.beans.formulaires;

import javax.servlet.http.HttpServletRequest;

import fr.eseo.ld.callisto.projetgl.beans.BeanException;
import fr.eseo.ld.callisto.projetgl.dao.DaoException;
import fr.eseo.ld.callisto.projetgl.dao.DaoFactory;

public class FormulaireChangerRoleUtilisateur {
	
	public static final String ROLE_ELEVE = "eleve";
	public static final String ROLE_INTERNE = "interne";
	public static final String ROLE_RESP_UE = "responsable-ue";
	public static final String ROLE_RESP_OPTION = "responsable-option";
	public static final String ROLE_RESP_MATIERE = "responsable-matiere";
	public static final String ROLE_ENCADRANT = "encadrant-matiere";
	
	private int idUtilisateur;
	private String role;
	private int idUe;
	private int idOption;
	private int idMatiere;
	private String message;

	public FormulaireChangerRoleUtilisateur(HttpServletRequest request) {
		idUtilisateur = Integer.parseInt(request.getParameter("listeUtilisateurs"));
		role = request.getParameter("roles");
		switch(role) {
		case ROLE_ELEVE:
		case ROLE_INTERNE:break;
		case ROLE_RESP_UE:
			idUe = Integer.parseInt(request.getParameter("listeUe"));
			break;
		case ROLE_RESP_OPTION:
			idOption = Integer.parseInt(request.getParameter("listeOptions"));
			break;
		case ROLE_RESP_MATIERE:
		case ROLE_ENCADRANT:
			idMatiere = Integer.parseInt(request.getParameter("listeMatieres"));
			break;
		}
	}
	
	public void valid() throws BeanException, DaoException {
		switch(role) {
		case ROLE_ELEVE:
			if(!DaoFactory.getImpl().estEleve(idUtilisateur)) {
				DaoFactory.getImpl().transformeRoleInterneVersEleve(idUtilisateur);
			}
			message = "L'utilisateur a pris le rôle d'élève";
			break;
		case ROLE_INTERNE:
			if(DaoFactory.getImpl().estEleve(idUtilisateur)) {
				DaoFactory.getImpl().transformeRoleEleveVersInterne(idUtilisateur);
			}
			message = "L'utilisateur a pris le rôle d'interne";
			break;
		case ROLE_RESP_UE:
			if(DaoFactory.getImpl().estEleve(idUtilisateur)) {
				throw new BeanException("Un élève ne peut pas être responsable d'une UE");
			}
			DaoFactory.getImpl().majResponsableUe(idUtilisateur, idUe);
			message = "L'utilisateur a pris le rôle de responsable d'UE";
			break;
		case ROLE_RESP_OPTION:
			if(DaoFactory.getImpl().estEleve(idUtilisateur)) {
				throw new BeanException("Un élève ne peut pas être responsable d'une option");
			}
			DaoFactory.getImpl().majResponsableOption(idUtilisateur, idOption);
			message = "L'utilisateur a pris le rôle de responsable d'option";
			break;
		case ROLE_RESP_MATIERE:
			if(DaoFactory.getImpl().estEleve(idUtilisateur)) {
				throw new BeanException("Un élève ne peut pas être responsable d'une matière");
			}
			DaoFactory.getImpl().majResponsableMatiere(idUtilisateur, idMatiere);
			message = "L'utilisateur a pris le rôle de responsable de matière";
			break;
		case ROLE_ENCADRANT:
			if(DaoFactory.getImpl().estEleve(idUtilisateur)) {
				throw new BeanException("Un élève ne peut pas être encadrant d'une matière");
			}
			DaoFactory.getImpl().ajouterEncadrantMatiere(idUtilisateur, idMatiere);
			message = "L'utilisateur a pris le rôle d'encadrant matière";
			break;
		}	
	}

	public String getMessage() {
		return message;
	}
	
}
