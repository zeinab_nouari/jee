package fr.eseo.ld.callisto.projetgl.beans;

//import javax.enterprise.inject.spi.Bean;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;


public class Utilisateur {

    public String nom;
    public String prenom;
    public String adresseMail;
    private int userId;
    private String motDePasse;
    private String login;
	private boolean eleve;

    public Utilisateur() {

    }
    
    public Utilisateur(int userId, String nom, String prenom, String adresseMail) throws BeanException {
        this.setUserId(userId);
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setAdresseMail(adresseMail);
    }

    public Utilisateur(int userId, String nom, String prenom, String adresseMail, String motDePasse, String login) throws BeanException {
        this.setUserId(userId);
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setAdresseMail(adresseMail);
        this.setMotDePasse(motDePasse);
        this.setLogin(login);
    }

    public Utilisateur(int idUtilisateur, String nom, String prenom, String adresseMail, String motDePasse, String login,
			boolean eleve) {
    	this.userId = idUtilisateur;
    	this.nom = nom;
    	this.prenom = prenom;
    	this.adresseMail = adresseMail;
    	this.motDePasse = motDePasse;
    	this.login = login;
    	this.eleve = eleve;
	}

	public String getNom() {
        return nom;
    }
 //TODO: exceptions bdd pour taille des champs entrés
    public void setNom(String nom) throws BeanException {
        if(nom.length() >= 15){
            throw new BeanException("taille trop longue");
        }
        else{
            this.nom = nom;
        }
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) throws BeanException{
        if(prenom.length() >= 15){
            throw new BeanException("taille trop longue");
        }
        else{
            this.prenom = prenom;
        }

    }

    public String getAdresseMail() {
        return adresseMail;
    }

    public void setAdresseMail(String adresseMail) throws BeanException {
        if(isValidEmailAddress(adresseMail)){
            this.adresseMail = adresseMail;
        }
        else{
            throw new BeanException("email non valide");
        }
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return "Utilisateur{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresseMail='" + adresseMail + '\'' +
                '}';
    }

    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }
    
}
