package fr.eseo.ld.callisto.projetgl.beans;

public class Option {
    private int idOption;
    private String nomOption;
    private int idResponsableOption;
	private Utilisateur responsableOption;

    public Option() {
    }

    public Option(int idOption, String nomOption, int idResponsableOption) {
        this.idOption = idOption;
        this.nomOption = nomOption;
        this.idResponsableOption = idResponsableOption;
    }

    public Option(int idOption, String nomOption, Utilisateur responsable) {
    	this.idOption = idOption;
    	this.nomOption = nomOption;
    	this.responsableOption = responsable;
	}

	public int getIdOption() {
        return idOption;
    }

    public void setIdOption(int idOption) {
        this.idOption = idOption;
    }

    public String getNomOption() {
        return nomOption;
    }

    public void setNomOption(String nomOption) {
        this.nomOption = nomOption;
    }

    public int getIdResponsableOption() {
        return idResponsableOption;
    }

    public void setIdResponsableOption(int idResponsableOption) {
        this.idResponsableOption = idResponsableOption;
    }

	public Utilisateur getResponsableOption() {
		return responsableOption;
	}

	public void setResponsableOption(Utilisateur responsableOption) {
		this.responsableOption = responsableOption;
	}
    
}
