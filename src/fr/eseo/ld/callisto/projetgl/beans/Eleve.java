package fr.eseo.ld.callisto.projetgl.beans;

public class Eleve extends Utilisateur{
    private int idEleve;

    public Eleve(int userId, String nom, String prenom, String adresseMail, int idEleve) throws BeanException {
        super(userId, nom, prenom, adresseMail);
        this.idEleve = idEleve;
    }

    public Eleve(Utilisateur utilisateur) throws BeanException {
		// TODO Auto-generated constructor stub
    	super(utilisateur.getUserId(), utilisateur.getNom(), utilisateur.getPrenom(), utilisateur.getAdresseMail());
	}

	public int getIdEleve() {
        return idEleve;
    }

    public void setIdEleve(int idEleve) {
        this.idEleve = idEleve;
    }
}
