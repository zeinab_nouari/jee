package fr.eseo.ld.callisto.projetgl.beans;

@SuppressWarnings("serial")
public class BeanException extends Exception {
    public BeanException(String message) {
        super(message);
    }
}
