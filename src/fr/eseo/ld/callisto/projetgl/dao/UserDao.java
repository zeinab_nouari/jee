package fr.eseo.ld.callisto.projetgl.dao;

import java.util.List;

import fr.eseo.ld.callisto.projetgl.beans.BeanException;
import fr.eseo.ld.callisto.projetgl.beans.Matiere;
import fr.eseo.ld.callisto.projetgl.beans.Option;
import fr.eseo.ld.callisto.projetgl.beans.Ue;
import fr.eseo.ld.callisto.projetgl.beans.Utilisateur;

public interface UserDao {
	List<Utilisateur> getListeDeUtilisateurs() throws BeanException;
	Utilisateur getUtilisateur(String nom, String prenom) throws BeanException;
	List<Utilisateur> getListeUtilisateurs() throws DaoException;
	List<Option> getListeOptions() throws DaoException;
	List<Ue> getListeUe() throws DaoException;
	List<Matiere> getListeMatieres() throws DaoException;
	void majResponsableUe(int idUtilisateur, int idUe) throws DaoException;
	void majResponsableOption(int idUtilisateur, int idOption) throws DaoException;
	void majResponsableMatiere(int idUtilisateur, int idMatiere) throws DaoException;
	boolean estEleve(int idUtilisateur) throws DaoException;
	void ajouterEncadrantMatiere(int idUtilisateur, int idMatiere) throws DaoException;
	void transformeRoleInterneVersEleve(int idUtilisateur) throws DaoException;
	void transformeRoleEleveVersInterne(int idUtilisateur) throws DaoException;
}