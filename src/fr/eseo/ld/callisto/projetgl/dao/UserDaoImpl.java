package fr.eseo.ld.callisto.projetgl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.eseo.ld.callisto.projetgl.beans.BeanException;
import fr.eseo.ld.callisto.projetgl.beans.Matiere;
import fr.eseo.ld.callisto.projetgl.beans.Option;
import fr.eseo.ld.callisto.projetgl.beans.Ue;
import fr.eseo.ld.callisto.projetgl.beans.Utilisateur;

public class UserDaoImpl implements UserDao {
	
	private Connection connexion;

	public UserDaoImpl(Connection connexion) {
		this.connexion = connexion;
	}

	@Override
	public List<Utilisateur> getListeDeUtilisateurs() throws BeanException {
		 Statement statement = null;
	     ResultSet resultat = null;
	     List<Utilisateur> res = new ArrayList<Utilisateur>();
		try {
			statement = connexion.createStatement();
            resultat = statement.executeQuery("SELECT id, mdp, nom, prenom, bureau, service, poste, chef FROM Utilisateur;");
            while (resultat.next()) {
            	Utilisateur p = new Utilisateur(resultat.getInt("userId"), 
						resultat.getString("nom"), 
						resultat.getString("prenom"), 
						resultat.getString("adresseMail"), 
						resultat.getString("motDePasse"), 
						resultat.getString("login"));
                res.add(p);
            }	
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			// System.out.println( "Fermeture de l'objet ResultSet." );
			if (resultat != null) {
				try {
					resultat.close();
				} catch (SQLException ignore) {
					
				}
			}
			// System.out.println( "Fermeture de l'objet Statement." );
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException ignore) {
					
				}
			}
		}

		
		return res;
	}

	@Override
	public Utilisateur getUtilisateur(String nom, String prenom) throws BeanException {
		@SuppressWarnings("unused")
		Statement statement = null;
		ResultSet resultat = null;

		try {
			statement = connexion.createStatement();
			PreparedStatement preparedStatement = connexion.prepareStatement("SELECT id, mdp, nom, prenom, bureau, service, poste, chef FROM Utilisateur WHERE prenom = ? AND nom = ?;");
			preparedStatement.setString(1, prenom);
			preparedStatement.setString(2, nom);
			resultat = preparedStatement.executeQuery();

			while (resultat.next()) {
				Utilisateur p = new Utilisateur(resultat.getInt("userId"), 
						resultat.getString("nom"), 
						resultat.getString("prenom"), 
						resultat.getString("adresseMail"), 
						resultat.getString("motDePasse"), 
						resultat.getString("login"));
				return p;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public List<Utilisateur> getListeUtilisateurs() throws DaoException {
		try {
			List<Utilisateur> listeUtilisateurs = new ArrayList<Utilisateur>();
			PreparedStatement statement = connexion.prepareStatement(
					"SELECT u.idUtilisateur, u.nom, u.prenom, u.adresseMail, u.motDePasse, u.login, u.eleve "
					+ "FROM utilisateur u; ");
			ResultSet liste = statement.executeQuery();
			while(liste.next()) {
				listeUtilisateurs.add(new Utilisateur(liste.getInt("idUtilisateur"), 
						liste.getString("nom"), liste.getString("prenom"),
						liste.getString("adresseMail"), liste.getString("motDePasse"), 
						liste.getString("login"), liste.getBoolean("eleve")));
			}
			return listeUtilisateurs;
		} catch (SQLException e) {
			throw new DaoException("Impossible de récupérer les options");
		} finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de données");
            }
        }
	}

	@Override
	public List<Option> getListeOptions() throws DaoException {
		try {
			List<Option> listeOption = new ArrayList<Option>();
			PreparedStatement statement = connexion.prepareStatement(
					"SELECT op.idOption, op.nomOption,"
					+ "u.idUtilisateur, u.nom, u.prenom, u.adresseMail, u.motDePasse, u.login, u.eleve "
					+ "FROM `option` op LEFT JOIN utilisateur u ON op.responsableOption=u.idUtilisateur; ");
			ResultSet liste = statement.executeQuery();
			while(liste.next()) {
				int idUtilisateur = liste.getInt("idUtilisateur");
				Utilisateur responsable = liste.wasNull() ? 
						null 
						:
						new Utilisateur(idUtilisateur, liste.getString("nom"), liste.getString("prenom"),
								liste.getString("adresseMail"), liste.getString("motDePasse"), 
								liste.getString("login"), liste.getBoolean("eleve"));
				listeOption.add(new Option(liste.getInt("idOption"), liste.getString("nomOption"), responsable));
			}
			return listeOption;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DaoException("Impossible de récupérer les options");
		} finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de données");
            }
        }
	}
	
	@Override
	public List<Ue> getListeUe() throws DaoException {
		try {
			List<Ue> listeUe = new ArrayList<Ue>();
			PreparedStatement statement = connexion.prepareStatement(
					"SELECT ue.idUe, ue.nomUe,"
					+ "u.idUtilisateur, u.nom, u.prenom, u.adresseMail, u.motDePasse, u.login, u.eleve "
					+ "FROM `ue` LEFT JOIN utilisateur u ON ue.responsableUe=u.idUtilisateur; ");
			ResultSet liste = statement.executeQuery();
			while(liste.next()) {
				int idUtilisateur = liste.getInt("idUtilisateur");
				Utilisateur responsable = liste.wasNull() ? 
						null 
						:
						new Utilisateur(idUtilisateur, liste.getString("nom"), liste.getString("prenom"),
								liste.getString("adresseMail"), liste.getString("motDePasse"), 
								liste.getString("login"), liste.getBoolean("eleve"));
				listeUe.add(new Ue(liste.getInt("idUe"), liste.getString("nomUe"), responsable));
			}
			return listeUe;
		} catch (SQLException e) {
			throw new DaoException("Impossible de récupérer les UE");
		} finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de données");
            }
        }
	}

	@Override
	public List<Matiere> getListeMatieres() throws DaoException {
		try {
			List<Matiere> listeMatiere = new ArrayList<Matiere>();
			PreparedStatement statement = connexion.prepareStatement(
					"SELECT m.idMatiere, m.nomMatiere,"
					+ "u.idUtilisateur, u.nom, u.prenom, u.adresseMail, u.motDePasse, u.login, u.eleve "
					+ "FROM `matiere` m LEFT JOIN utilisateur u ON m.responsableMatiere=u.idUtilisateur; ");
			ResultSet liste = statement.executeQuery();
			while(liste.next()) {
				int idUtilisateur = liste.getInt("idUtilisateur");
				Utilisateur responsable = liste.wasNull() ? 
						null 
						:
						new Utilisateur(idUtilisateur, liste.getString("nom"), liste.getString("prenom"),
								liste.getString("adresseMail"), liste.getString("motDePasse"), 
								liste.getString("login"), liste.getBoolean("eleve"));
				listeMatiere.add(new Matiere(liste.getInt("idMatiere"), liste.getString("nomMatiere"), responsable));
			}
			return listeMatiere;
		} catch (SQLException e) {
			throw new DaoException("Impossible de récupérer les matières");
		} finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de données");
            }
        }
	}
	
	@Override
	public boolean estEleve(int idUtilisateur) throws DaoException {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connexion.prepareStatement("SELECT eleve FROM utilisateur WHERE idUtilisateur=? ;");
			preparedStatement.setInt(1, idUtilisateur);
			ResultSet resultat = preparedStatement.executeQuery();

			while (resultat.next()) {
				return resultat.getBoolean("eleve");
			}
			throw new DaoException("Utilisateur introuvable");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DaoException("Erreur lors de la recherche de l'utilisateur");
		} finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de données");
            }
        }
	}
	
	@Override
	public void majResponsableUe(int idUtilisateur, int idUe) throws DaoException {
		try {
			PreparedStatement pStatement = connexion.prepareStatement("UPDATE ue SET responsableUe=? WHERE idUe=? ;");
			pStatement.setInt(1, idUtilisateur);
			pStatement.setInt(2, idUe);
			pStatement.executeUpdate();
			connexion.commit();
		} catch (SQLException e) {
			try {
                if (connexion != null) {
                    connexion.rollback();
                }
            } catch (SQLException e2) {}
			throw new DaoException("Erreur de mise à jour du responsable d'UE");
		} finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de données");
            }
        }
	}
	
	@Override
	public void majResponsableOption(int idUtilisateur, int idOption) throws DaoException {
		try {
			PreparedStatement pStatement = connexion.prepareStatement(
					"UPDATE `option` SET responsableOption=? WHERE idOption=? ;");
			pStatement.setInt(1, idUtilisateur);
			pStatement.setInt(2, idOption);
			pStatement.executeUpdate();
			connexion.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
                if (connexion != null) {
                    connexion.rollback();
                }
            } catch (SQLException e2) {}
			throw new DaoException("Erreur de mise à jour du responsable d'option");
		} finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de données");
            }
        }
	}
	
	@Override
	public void majResponsableMatiere(int idUtilisateur, int idMatiere) throws DaoException {
		try {
			PreparedStatement pStatement = connexion.prepareStatement(
					"UPDATE matiere SET responsableMatiere=? WHERE idMatiere=? ;");
			pStatement.setInt(1, idUtilisateur);
			pStatement.setInt(2, idMatiere);
			pStatement.executeUpdate();
			connexion.commit();
		} catch (SQLException e) {
			try {
                if (connexion != null) {
                    connexion.rollback();
                }
            } catch (SQLException e2) {}
			throw new DaoException("Erreur de mise à jour du responsable de la matière");
		} finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de données");
            }
        }
	}
	
	@Override
	public void ajouterEncadrantMatiere(int idUtilisateur, int idMatiere) throws DaoException {
		try {
			PreparedStatement pStatementAdd = connexion.prepareStatement(
					"INSERT INTO encadrant (idMatiere, idUtilisateur) VALUES (?, ?)");
			pStatementAdd.setInt(1, idUtilisateur);
			pStatementAdd.setInt(2, idMatiere);
			pStatementAdd.executeUpdate();
			connexion.commit();
		} catch (SQLException e) {
			try {
                if (connexion != null) {
                    connexion.rollback();
                }
            } catch (SQLException e2) {}
			throw new DaoException("Erreur lors de l'ajout de l'encadrant à la matière");
		} finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de données");
            }
        }
	}
	
	@Override
	public void transformeRoleEleveVersInterne(int idUtilisateur) throws DaoException {
		try {
			PreparedStatement pStatementStatus = connexion.prepareStatement(
					"UPDATE utilisateur SET eleve=0 WHERE idUtilisateur=? ;");
			pStatementStatus.setInt(1, idUtilisateur);
			pStatementStatus.executeUpdate();
			//Retirer des éventuels groupes d'élèves
			PreparedStatement pStatementGroup = connexion.prepareStatement(
					"DELETE FROM estdansgroupetravail WHERE utilisateur=? ;");
			pStatementGroup.setInt(1, idUtilisateur);
			pStatementGroup.executeUpdate();
			connexion.commit();
		} catch (SQLException e) {
			try {
                if (connexion != null) {
                    connexion.rollback();
                }
            } catch (SQLException e2) {}
			throw new DaoException("L'élève n'a pas pu prendre le role d'interne");
		} finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de données");
            }
        }
	}
	
	@Override
	public void transformeRoleInterneVersEleve(int idUtilisateur) throws DaoException {
		try {
			PreparedStatement pStatementStatus = connexion.prepareStatement(
					"UPDATE utilisateur SET eleve=1 WHERE idUtilisateur=? ;");
			pStatementStatus.setInt(1, idUtilisateur);
			pStatementStatus.executeUpdate();
			//Retirer les éventuels responsabilités
			PreparedStatement psRetireUe = connexion.prepareStatement(
					"UPDATE ue SET responsableUe=null WHERE responsableUe=? ");
			psRetireUe.setInt(1, idUtilisateur);
			psRetireUe.executeUpdate();
			
			PreparedStatement psRetireOption = connexion.prepareStatement(
					"UPDATE `option` SET responsableOption=null WHERE responsableOption=? ");
			psRetireOption.setInt(1, idUtilisateur);
			psRetireOption.executeUpdate();
			
			PreparedStatement psRetireMatiere = connexion.prepareStatement(
					"UPDATE matiere SET responsableMatiere=null WHERE responsableMatiere=? ");
			psRetireMatiere.setInt(1, idUtilisateur);
			psRetireMatiere.executeUpdate();
			
			PreparedStatement psEncadre = connexion.prepareStatement(
					"DELETE FROM Encadre WHERE utilisateur=? ;");
			psEncadre.setInt(1, idUtilisateur);
			psEncadre.executeUpdate();
			
			PreparedStatement psAudit = connexion.prepareStatement(
					"UPDATE audit SET idEncadrant=null WHERE idEncadrant=? AND dateAudit>SYSDATE()");
			psAudit.setInt(1, idUtilisateur);
			
			connexion.commit();
		} catch (SQLException e) {
			try {
                if (connexion != null) {
                    connexion.rollback();
                }
            } catch (SQLException e2) {}
			throw new DaoException("L'interne n'a pas pu prendre le role d'élève");
		} finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de données");
            }
        }
	}
	
}