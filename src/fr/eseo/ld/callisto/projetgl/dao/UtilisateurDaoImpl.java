package fr.eseo.ld.callisto.projetgl.dao;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.eseo.ld.callisto.projetgl.beans.Audit;
import fr.eseo.ld.callisto.projetgl.beans.BeanException;
import fr.eseo.ld.callisto.projetgl.beans.Eleve;
import fr.eseo.ld.callisto.projetgl.beans.Encadrant;
import fr.eseo.ld.callisto.projetgl.beans.Groupe;
import fr.eseo.ld.callisto.projetgl.beans.Matiere;
import fr.eseo.ld.callisto.projetgl.beans.Responsable;
import fr.eseo.ld.callisto.projetgl.beans.Utilisateur;
import fr.eseo.ld.callisto.projetgl.utils.HashClass;



public class UtilisateurDaoImpl implements UtilisateurDao {
	
	private Connection connexion;

	public UtilisateurDaoImpl(Connection connexion) {
		this.connexion = connexion;
	}

	@Override
	public List<Utilisateur> getListUtilisateurs() throws DaoException {
		 Statement statement = null;
	     ResultSet resultat = null;
	     List<Utilisateur> res = new ArrayList<Utilisateur>();
		try {
			statement = connexion.createStatement();
            resultat = statement.executeQuery("SELECT userId, nom, prenom, adresseMail FROM Utilisateur;");
            while (resultat.next()) {
            	Utilisateur p = new Utilisateur(resultat.getInt("userId"), 
						resultat.getString("nom"), 
						resultat.getString("prenom"), 
						resultat.getString("adresseMail"));
                res.add(p);
            }
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (BeanException e) {
			throw new DaoException(e.getMessage());
		}finally {
			// System.out.println( "Fermeture de l'objet ResultSet." );
			if (resultat != null) {
				try {
					resultat.close();
				} catch (SQLException ignore) {
					
				}
			}
			// System.out.println( "Fermeture de l'objet Statement." );
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException ignore) {
					
				}
			}
		}
		return res;
	}
	
	@Override
	public List<Encadrant> getListeEncadrants(Matiere matiere) throws DaoException {
		Statement statement = null;
	     ResultSet resultat = null;
	     List<Encadrant> res = new ArrayList<Encadrant>();
		try {
			PreparedStatement preparedStatement = connexion.prepareStatement(""
					+ "SELECT utilisateur.idUtilisateur,matiere.idMatiere"
					+ "FROM utilisateur,encadrant,matiere WHERE ? = encadrant.idMatiere"
					+ "&& encadrant.idUtilisateur = utilisateur.idUtilisateur;");
			preparedStatement.setInt(1, matiere.getIdMatiere());
			resultat = preparedStatement.executeQuery();
           
           while (resultat.next()) {
        	   List<Utilisateur> utilisateurs = getListUtilisateurs();
        	   Encadrant encadrant = new Encadrant(utilisateurs.get(resultat.getInt("idUtilisateur")));
               res.add(encadrant);
           }
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (BeanException e) {
			throw new DaoException(e.getMessage());
		}finally {
			// System.out.println( "Fermeture de l'objet ResultSet." );
			if (resultat != null) {
				try {
					resultat.close();
				} catch (SQLException ignore) {
					
				}
			}
			// System.out.println( "Fermeture de l'objet Statement." );
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException ignore) {
					
				}
			}
		}
		return res;
	}
	
	@Override
	public Utilisateur getUtilisateur(String nom, String prenom) throws DaoException {
		@SuppressWarnings("unused")
		Statement statement = null;
		ResultSet resultat = null;

		try {
			statement = connexion.createStatement();
			PreparedStatement preparedStatement = connexion.prepareStatement("SELECT idUtilisateur, nom, prenom, adresseMail FROM Utilisateur WHERE nom=? and prenom=?;");
			preparedStatement.setString(1, nom);
			preparedStatement.setString(2, prenom);
			resultat = preparedStatement.executeQuery();

			while (resultat.next()) {
				Utilisateur p = new Utilisateur(resultat.getInt("idUtilisateur"), 
						resultat.getString("nom"), 
						resultat.getString("prenom"), 
						resultat.getString("adresseMail"));
				return p;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (BeanException e) {
			throw new DaoException(e.getMessage());
		}
		return null;
	}
	
	@Override
	public boolean estResponsable(Utilisateur utilisateur) throws DaoException {
		@SuppressWarnings("unused")
		Statement statement = null;
		ResultSet resultat = null;
		
		try {
			statement = connexion.createStatement();
			PreparedStatement preparedStatement = connexion.prepareStatement("Select count(Distinct Responsable.utilisateurId) as psc FROM Responsable WHERE EXISTS (SELECT * FROM Responsable WHERE utilisateurId = ? );");
			preparedStatement.setInt(1, utilisateur.getUserId());
			resultat = preparedStatement.executeQuery();

			while (resultat.next()) {
				if(resultat.getInt("psc")!=0)
					return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean estResponsableOption(Utilisateur utilisateur) throws DaoException {
		@SuppressWarnings("unused")
		Statement statement = null;
		ResultSet resultat = null;
		
		try {
			statement = connexion.createStatement();
			PreparedStatement preparedStatement = connexion.prepareStatement("Select count(Distinct Option.responsableOption) as psc FROM Option WHERE EXISTS (SELECT * FROM Option WHERE responsableOption = ? );");
			preparedStatement.setInt(1, utilisateur.getUserId());
			resultat = preparedStatement.executeQuery();

			while (resultat.next()) {
				if(resultat.getInt("psc")!=0)
					return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean estEncadrant(Utilisateur utilisateur) throws DaoException {
		@SuppressWarnings("unused")
		Statement statement = null;
		ResultSet resultat = null;
		
		try {
			statement = connexion.createStatement();
			PreparedStatement preparedStatement = connexion.prepareStatement("Select count(Distinct Encadrant.utilisateurId) as psc FROM Encadrant WHERE EXISTS (SELECT * FROM Encadrant WHERE utilisateurId = ? );");
			preparedStatement.setInt(1, utilisateur.getUserId());
			resultat = preparedStatement.executeQuery();

			while (resultat.next()) {
				if(resultat.getInt("psc")!=0)
					return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	

	@Override
	public boolean estEleve(Utilisateur utilisateur) throws DaoException {
		@SuppressWarnings("unused")
		Statement statement = null;
		ResultSet resultat = null;
		
		try {
			statement = connexion.createStatement();
			PreparedStatement preparedStatement = connexion.prepareStatement("select eleve from utilisateur where idUtilisateur = ? ;");
			preparedStatement.setInt(1, utilisateur.getUserId());
			resultat = preparedStatement.executeQuery();

			while (resultat.next()) {
				if(resultat.getBoolean("eleve")==true)
					return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public Responsable getResponsable(Utilisateur utilisateur) throws DaoException, BeanException {
		@SuppressWarnings("unused")
		Statement statement = null;
		ResultSet resultat = null;
		Responsable responsable=null;
		try {
			statement = connexion.createStatement();
			PreparedStatement preparedStatement = connexion.prepareStatement("Select Responsable.idResponsable as id FROM Responsable WHERE utilisateurId = ? ;");
			preparedStatement.setInt(1, utilisateur.getUserId());
			resultat = preparedStatement.executeQuery();

			while (resultat.next()) {
				responsable = new Responsable(utilisateur);
				responsable.setIdResponsable(resultat.getInt("id"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return responsable;
	}
	
	@Override
	public Encadrant getEncadrant(Utilisateur utilisateur) throws DaoException, BeanException {
		@SuppressWarnings("unused")
		Statement statement = null;
		ResultSet resultat = null;
		Encadrant encadrant=null;
		try {
			statement = connexion.createStatement();
			PreparedStatement preparedStatement = connexion.prepareStatement("Select encadrant.idEncadrant as id FROM encadrant WHERE utilisateurId = ? ;");
			preparedStatement.setInt(1, utilisateur.getUserId());
			resultat = preparedStatement.executeQuery();
			while (resultat.next()) {
				encadrant = new Encadrant(utilisateur);
				encadrant.setIdEncadrant(resultat.getInt("id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return encadrant;
	}
	
	


	@Override
	public Eleve getEleve(Utilisateur utilisateur) throws DaoException, BeanException {
		@SuppressWarnings("unused")
		Statement statement = null;
		ResultSet resultat = null;
		Eleve eleve=null;
		try {
			statement = connexion.createStatement();
			PreparedStatement preparedStatement = connexion.prepareStatement("Select * FROM utilisateur WHERE idUtilisateur = ? ;");
			preparedStatement.setInt(1, utilisateur.getUserId());
			resultat = preparedStatement.executeQuery();
			while (resultat.next()) {
				eleve = new Eleve(utilisateur);
				eleve.setIdEleve(resultat.getInt("idUtilisateur"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return eleve;
	}
	
	@Override
    public List<Audit> getListeAuditsByEncadrant(Encadrant encadrant) throws DaoException {
         ResultSet resultat = null;
         List<Audit> audits = new ArrayList<Audit>();
         try {   	
            PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM Audit WHERE idEncadrant = ? ;");
            preparedStatement.setInt(1, encadrant.getIdEncadrant());
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Audit audit = null;
				try {
					audit = new Audit(resultat.getInt("idAudit"), 
					        resultat.getString("nom"), 
					        resultat.getString("description"), 
					        resultat.getDate("dateCreation"), 
					        resultat.getDate("dateAudit"), 
					        resultat.getString("etat"));
				} catch (BeanException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                audits.add(audit);
                
            }
            return audits ;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
	
	
	

	@Override
	public boolean checkMotDePasse(Utilisateur utilisateur) throws DaoException {
		boolean value=false;
        Statement statement = null;
        ResultSet resultat = null;
        try {
        	String hash = HashClass.hashIt(utilisateur.getMotDePasse(), utilisateur.getPrenom(), 100);
            statement = connexion.createStatement();
            resultat = statement.executeQuery("SELECT * FROM personne WHERE EXISTS (SELECT * FROM personne WHERE nom = '"+utilisateur.getNom()+"' and prenom = '"+utilisateur.getPrenom()+"' and mdp = '"+hash+"');");
            while (resultat.next()) {
            	value=true;
           }
        } catch (SQLException e) {
            throw new DaoException("Impossible de communiquer avec la base de donn�es");
        } catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de donn�es");
            }
        }
		return value;
	} 
	
	@Override
	public Audit getAuditById(int id) throws DaoException {
		
		ResultSet resultat = null;
		ResultSet resultat2 = null;
		Audit audit = null;
		List <Encadrant> encadrants = new ArrayList<Encadrant>() ;
		List <Eleve> eleves = new ArrayList<Eleve>();
		try {   	
            PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM Audit WHERE idAudit = ? ;");
            preparedStatement.setInt(1, id);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {
                
				try {
					audit = new Audit(resultat.getInt("idAudit"), 
					        resultat.getString("nom"), 
					        resultat.getString("description"), 
					        resultat.getDate("dateCreation"), 
					        resultat.getDate("dateAudit"), 
					        resultat.getString("etat"));
					audit.setMatiere(this.getMatiere(resultat.getInt("idMatiere")));
					
				} catch (BeanException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				PreparedStatement preparedStatement2 = connexion.prepareStatement("SELECT * FROM VueAudit WHERE idAudit = ? ;");
	            preparedStatement2.setInt(1, id);
	            resultat2 = preparedStatement2.executeQuery();
	            while(resultat2.next()) {
	            	
	            	try {
						String nomEnc = resultat2.getString("nomEncadrant");
						String prenomEnc = resultat2.getString("prenomEncadrant");
						Encadrant encadrant = this.getEncadrant(this.getUtilisateur(nomEnc, prenomEnc));
						Eleve eleve = this.getEleve(this.getUtilisateur(resultat2.getString("nomEleve"), resultat2.getString("prenomEleve")));
						Responsable responsableMatiere = this.getResponsable(this.getUtilisateur(resultat2.getString("nomRespMatiere"), resultat2.getString("prenomRespMatiere"))) ;
						
						eleves.add(eleve);
						audit.setListeEleves(eleves);
						audit.setEncadrant(encadrant);
						audit.setGroupe(this.getGroupe(resultat2.getInt("idGroupe")));
						audit.setResponsableMatiere(responsableMatiere);
						
					} catch (BeanException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            }

            }
            return audit ;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

	@Override
	public Matiere getMatiere(int id) throws DaoException {
		ResultSet resultat = null;
		Matiere matiere = null ;
		try {   	
            PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM Matiere matiere, Audit audit WHERE matiere.idMatiere = ? ;");
            preparedStatement.setInt(1, id);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {
                
				matiere = new Matiere(resultat.getInt("idMatiere"),resultat.getString("nomMatiere"),resultat.getInt("responsableMatiere"));
            }
            return matiere ;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
	}

	@Override
	public Groupe getGroupe(int id) throws DaoException {
		ResultSet resultat = null;
		Groupe groupe = null ;
		try {   	
            PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM Groupe  WHERE idGroupe = ? ;");
            preparedStatement.setInt(1, id);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {
                
				groupe = new Groupe(resultat.getInt("idGroupe"),resultat.getString("description"),resultat.getInt("typeDeGroupe"));
            }
            return groupe ;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
	}

	@Override
	public List<Audit> getListeAuditsByEleve(Eleve eleve) throws DaoException {
		ResultSet resultat = null;
        List<Audit> audits = new ArrayList<Audit>();
        try {   	
           PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM vueAudit WHERE idEleve = ? ;");
           preparedStatement.setInt(1, eleve.getIdEleve());
           resultat = preparedStatement.executeQuery();
           while (resultat.next()) {
        	   Audit audit = null;
               audit = this.getAuditById(resultat.getInt("idAudit"));
               audits.add(audit);
           }
           return audits ;
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return null;
	}

	@Override
	public List<Audit> getListeAuditsByRespMatiere(Responsable responsable) throws DaoException {
		Responsable resp = responsable ;
		ResultSet resultat = null;
        List<Audit> audits = new ArrayList<Audit>();
        try {   	
           PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM Audit a, Matiere m WHERE a.idMatiere = m.idMatiere AND m.responsableMatiere = ? ;");
           preparedStatement.setInt(1, responsable.getUserId());
           resultat = preparedStatement.executeQuery();
           while (resultat.next()) {
        	   Audit audit = null;
               audit = this.getAuditById(resultat.getInt("idAudit"));
               audits.add(audit);
           }
           return audits ;
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return null;
	}
	
	
	
	@Override
	public List<Audit> getListeAuditsByRespOption(Responsable responsable) throws DaoException {
		ResultSet resultat = null;
		ResultSet resultat2 = null;
		
        List<Audit> audits = new ArrayList<Audit>();
        int idRespOption = 0 ;
        try {   	
           PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM Option o, utilisateur u WHERE u.idUtilisateur = ? and o.responsableOption = u.idUtilisateur ;");
           preparedStatement.setInt(1, responsable.getUserId());
           resultat = preparedStatement.executeQuery();
           if(resultat.next()) {
        	   try {
        	  PreparedStatement preparedStatement2 = connexion.prepareStatement("SELECT * FROM vueaudit v WHERE v.OptionEleve = ? ;");
              preparedStatement2.setInt(1, idRespOption);
              resultat2 = preparedStatement2.executeQuery();
        	  while(resultat2.next()) {
        		  Audit audit = new Audit();
        		  audit = this.getAuditById(resultat.getInt("idAudit"));
        		  audits.add(audit);
        	  }
        	  return audits ; }
        	   catch (SQLException e) {
                   e.printStackTrace();
               }
        	   
           }
   
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return null;
	}
	}

	
	
