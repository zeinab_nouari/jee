package fr.eseo.ld.callisto.projetgl.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DaoFactory {

	private final static String BDD_LOGIN = "root";
	private final static String BDD_PWD = "";

	private String url;
	private String username;
	private String password;

	DaoFactory(String url, String username, String password) {
		this.url = url;
		this.username = username;
		this.password = password;
	}
	public static DaoFactory getInstance() throws DaoException {
		try {

			Class.forName("org.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {

		}

		DaoFactory instance = new DaoFactory(
				"jdbc:mariadb://localhost:3307/projets8-callisto?useUnicode=true&characterEncoding=utf8", "root", "");


		// connexion BD
		Connection connexion = null;
		try {
			connexion = DriverManager.getConnection("jdbc:mariadb://localhost:3307/projet?useUnicode=true&characterEncoding=utf8", BDD_LOGIN, BDD_PWD);
			connexion.setAutoCommit(false);
		} catch (SQLException e2) {
			//e2.printStackTrace();
			throw new DaoException("Impossible de se connecter à la base de données");

		}
		return instance;}

	public Connection getConnection() throws SQLException {
		Connection connexion=DriverManager.getConnection(url, username, password);
		connexion.setAutoCommit(false);
		return connexion;
	}
	
	static public UserDao getImpl() throws DaoException {
		return getImplBDD();
	}
	
	static public UtilisateurDao getImpl2() throws DaoException {
		return getImplUtilisateur();
	}
	
	/**
	 * Implémentation BD localhost
	 * 
	 * @return
	 * @throws DaoException
	 */
	static private UserDao getImplBDD() throws DaoException {

		// load driver
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			//Class.forName("com.mysql.jdbc.Driver");
			// System.out.println("Driver chargé !");
		} catch (ClassNotFoundException e1) {
			//e1.printStackTrace();
			throw new DaoException("Impossible de se connecter à la base de données");
			
		}

		// connexion BD
		Connection connexion = null;
		try {
			connexion = DriverManager.getConnection("jdbc:mariadb://localhost:3307/projet?useUnicode=true&characterEncoding=utf8", BDD_LOGIN, BDD_PWD);
			connexion.setAutoCommit(false);
		} catch (SQLException e2) {
			//e2.printStackTrace();
			throw new DaoException("Impossible de se connecter à la base de données");
			
		}
		return new UserDaoImpl(connexion);
	}
	
	
	static private UtilisateurDao getImplUtilisateur() throws DaoException {

		// load driver
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			//Class.forName("com.mysql.jdbc.Driver");
			// System.out.println("Driver chargé !");
		} catch (ClassNotFoundException e1) {
			//e1.printStackTrace();
			throw new DaoException("Impossible de se connecter à la base de données");
			
		}

		// connexion BD
		Connection connexion = null;
		try {
			connexion = DriverManager.getConnection("jdbc:mariadb://localhost:3307/s8-callisto?useUnicode=true&characterEncoding=utf8", BDD_LOGIN, BDD_PWD);
			connexion.setAutoCommit(false);
		} catch (SQLException e2) {
			//e2.printStackTrace();
			throw new DaoException("Impossible de se connecter à la base de données");
			
		}
		return new UtilisateurDaoImpl(connexion);
	}

}
