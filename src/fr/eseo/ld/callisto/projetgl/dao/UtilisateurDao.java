package fr.eseo.ld.callisto.projetgl.dao;

import java.util.List;

import fr.eseo.ld.callisto.projetgl.beans.Audit;
import fr.eseo.ld.callisto.projetgl.beans.BeanException;
import fr.eseo.ld.callisto.projetgl.beans.Eleve;
import fr.eseo.ld.callisto.projetgl.beans.Encadrant;
import fr.eseo.ld.callisto.projetgl.beans.Groupe;
import fr.eseo.ld.callisto.projetgl.beans.Matiere;
import fr.eseo.ld.callisto.projetgl.beans.Responsable;
import fr.eseo.ld.callisto.projetgl.beans.Utilisateur;


public interface UtilisateurDao {
	List<Utilisateur> getListUtilisateurs() throws DaoException;
	Utilisateur getUtilisateur(String nom, String prenom) throws DaoException;
	boolean estResponsable(Utilisateur utilisateur) throws DaoException;
	Responsable getResponsable(Utilisateur utilisateur) throws DaoException, BeanException;
	boolean estEncadrant(Utilisateur utilisateur) throws DaoException;
	Encadrant getEncadrant(Utilisateur utilisateur) throws DaoException, BeanException;
	List<Audit> getListeAuditsByEncadrant(Encadrant encadrant) throws DaoException;
	boolean checkMotDePasse(Utilisateur utilisateur) throws DaoException;
	List<Encadrant> getListeEncadrants(Matiere matiere) throws DaoException;
	Audit getAuditById(int id) throws DaoException ;
	Eleve getEleve(Utilisateur utilisateur) throws DaoException, BeanException;
	boolean estEleve(Utilisateur utilisateur) throws DaoException;
	Matiere getMatiere(int id) throws DaoException ;
	Groupe getGroupe(int id) throws DaoException ;
	List <Audit> getListeAuditsByEleve(Eleve eleve) throws DaoException ;
	List <Audit> getListeAuditsByRespMatiere(Responsable responsable) throws DaoException ;
	List<Audit> getListeAuditsByRespOption(Responsable responsable) throws DaoException;
	boolean estResponsableOption(Utilisateur utilisateur) throws DaoException;
}