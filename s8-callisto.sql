-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3307
-- Généré le : sam. 24 avr. 2021 à 14:15
-- Version du serveur :  10.4.13-MariaDB
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `s8-callisto`
--

-- --------------------------------------------------------

--
-- Structure de la table `audit`
--

DROP TABLE IF EXISTS `audit`;
CREATE TABLE IF NOT EXISTS `audit` (
  `idAudit` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) DEFAULT NULL,
  `nom` varchar(40) DEFAULT NULL,
  `dateCreation` date DEFAULT NULL,
  `dateAudit` date DEFAULT NULL,
  `etat` enum('cree','planifie','en cours','termine','archive') DEFAULT NULL,
  `idEncadrant` int(11) DEFAULT NULL,
  `idMatiere` int(11) DEFAULT NULL,
  `idModeleAudit` int(11) DEFAULT NULL,
  PRIMARY KEY (`idAudit`),
  KEY `idEncadrant` (`idEncadrant`),
  KEY `idModeleAudit` (`idModeleAudit`),
  KEY `idMatiere` (`idMatiere`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `audit`
--

INSERT INTO `audit` (`idAudit`, `description`, `nom`, `dateCreation`, `dateAudit`, `etat`, `idEncadrant`, `idMatiere`, `idModeleAudit`) VALUES
(1, 'Audit de bases de données relationelle', 'BDD', '2021-02-22', '2021-03-22', 'planifie', 1, 2, 1),
(2, 'Audit de test ', 'Test', '2021-04-13', '2021-04-22', 'planifie', 1, 1, 1),
(3, 'Audit de bdd', 'BDD', '2021-04-13', '2021-04-22', 'cree', 1, 1, 1),
(4, 'matière 1', 'BDD 2', '2021-04-13', '2021-04-01', 'termine', 1, 1, 1),
(5, 'Audit de test', 'Audit 2', '2021-04-13', '2021-04-22', 'cree', 2, 4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `auditesur`
--

DROP TABLE IF EXISTS `auditesur`;
CREATE TABLE IF NOT EXISTS `auditesur` (
  `idAudit` int(11) DEFAULT NULL,
  `idGroupeAudit` int(11) DEFAULT NULL,
  `idReponse` int(11) DEFAULT NULL,
  KEY `idAudit` (`idAudit`),
  KEY `idReponse` (`idReponse`),
  KEY `idGroupeAudit` (`idGroupeAudit`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `auditesur`
--

INSERT INTO `auditesur` (`idAudit`, `idGroupeAudit`, `idReponse`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `contientgroupe`
--

DROP TABLE IF EXISTS `contientgroupe`;
CREATE TABLE IF NOT EXISTS `contientgroupe` (
  `eleve` int(11) DEFAULT NULL,
  `groupe` int(11) DEFAULT NULL,
  KEY `eleve` (`eleve`),
  KEY `groupe` (`groupe`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `contientgroupe`
--

INSERT INTO `contientgroupe` (`eleve`, `groupe`) VALUES
(7, 2),
(6, 2),
(3, 2),
(5, 2),
(2, 2),
(1, 3),
(4, 3),
(8, 3),
(7, 4),
(6, 4),
(3, 4),
(5, 4),
(2, 4),
(1, 4),
(4, 4),
(8, 4),
(9, 5);

-- --------------------------------------------------------

--
-- Structure de la table `contientquestion`
--

DROP TABLE IF EXISTS `contientquestion`;
CREATE TABLE IF NOT EXISTS `contientquestion` (
  `idQuestion` int(11) DEFAULT NULL,
  `idModeleAudit` int(11) DEFAULT NULL,
  `questionPourEleve` int(1) DEFAULT NULL,
  `odre` int(11) DEFAULT NULL,
  KEY `idQuestion` (`idQuestion`),
  KEY `idModeleAudit` (`idModeleAudit`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `contientquestion`
--

INSERT INTO `contientquestion` (`idQuestion`, `idModeleAudit`, `questionPourEleve`, `odre`) VALUES
(1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `contient_eleve_audit`
--

DROP TABLE IF EXISTS `contient_eleve_audit`;
CREATE TABLE IF NOT EXISTS `contient_eleve_audit` (
  `idAudit` int(11) DEFAULT NULL,
  `idEleve` int(11) DEFAULT NULL,
  `idQuestion` int(11) DEFAULT NULL,
  `idGroupe` int(11) DEFAULT NULL,
  `reponse` varchar(255) DEFAULT NULL,
  KEY `idAudit` (`idAudit`),
  KEY `idEleve` (`idEleve`),
  KEY `idGroupe` (`idGroupe`),
  KEY `idQuestion` (`idQuestion`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `contient_eleve_audit`
--

INSERT INTO `contient_eleve_audit` (`idAudit`, `idEleve`, `idQuestion`, `idGroupe`, `reponse`) VALUES
(1, 1, 1, 1, NULL),
(1, 2, 1, 1, NULL),
(2, 5, NULL, NULL, NULL),
(5, 8, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `contient_ue_matiere`
--

DROP TABLE IF EXISTS `contient_ue_matiere`;
CREATE TABLE IF NOT EXISTS `contient_ue_matiere` (
  `matiere` int(11) DEFAULT NULL,
  `ue` int(11) DEFAULT NULL,
  KEY `matiere` (`matiere`),
  KEY `ue` (`ue`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `contient_ue_matiere`
--

INSERT INTO `contient_ue_matiere` (`matiere`, `ue`) VALUES
(1, 2),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `contient_ue_option`
--

DROP TABLE IF EXISTS `contient_ue_option`;
CREATE TABLE IF NOT EXISTS `contient_ue_option` (
  `option` int(11) DEFAULT NULL,
  `ue` int(11) DEFAULT NULL,
  KEY `option` (`option`),
  KEY `ue` (`ue`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `contient_ue_option`
--

INSERT INTO `contient_ue_option` (`option`, `ue`) VALUES
(1, 1),
(1, 2),
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `encadrant`
--

DROP TABLE IF EXISTS `encadrant`;
CREATE TABLE IF NOT EXISTS `encadrant` (
  `idEncadrant` int(11) NOT NULL AUTO_INCREMENT,
  `utilisateurId` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEncadrant`),
  KEY `utilisateurId` (`utilisateurId`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `encadrant`
--

INSERT INTO `encadrant` (`idEncadrant`, `utilisateurId`) VALUES
(1, 3),
(2, 4),
(4, 6);

-- --------------------------------------------------------

--
-- Structure de la table `encadre`
--

DROP TABLE IF EXISTS `encadre`;
CREATE TABLE IF NOT EXISTS `encadre` (
  `idMatiere` int(11) DEFAULT NULL,
  `idUtilisateur` int(11) DEFAULT NULL,
  KEY `idMatiere` (`idMatiere`),
  KEY `idUtilisateur` (`idUtilisateur`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `encadre`
--

INSERT INTO `encadre` (`idMatiere`, `idUtilisateur`) VALUES
(1, 3),
(2, 4);

-- --------------------------------------------------------

--
-- Structure de la table `encadre_encadrant_matiere`
--

DROP TABLE IF EXISTS `encadre_encadrant_matiere`;
CREATE TABLE IF NOT EXISTS `encadre_encadrant_matiere` (
  `matiere` int(11) DEFAULT NULL,
  `encadrant` int(11) DEFAULT NULL,
  KEY `matiere` (`matiere`),
  KEY `encadrant` (`encadrant`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `encadre_encadrant_matiere`
--

INSERT INTO `encadre_encadrant_matiere` (`matiere`, `encadrant`) VALUES
(1, 2),
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `estdansgroupeaudit`
--

DROP TABLE IF EXISTS `estdansgroupeaudit`;
CREATE TABLE IF NOT EXISTS `estdansgroupeaudit` (
  `idUtilisateur` int(11) DEFAULT NULL,
  `idGroupeAudit` int(11) DEFAULT NULL,
  KEY `idUtilisateur` (`idUtilisateur`),
  KEY `idGroupeAudit` (`idGroupeAudit`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `estdansgroupeaudit`
--

INSERT INTO `estdansgroupeaudit` (`idUtilisateur`, `idGroupeAudit`) VALUES
(1, 1),
(7, 1),
(8, 1);

-- --------------------------------------------------------

--
-- Structure de la table `estdansgroupetravail`
--

DROP TABLE IF EXISTS `estdansgroupetravail`;
CREATE TABLE IF NOT EXISTS `estdansgroupetravail` (
  `idUtilisateur` int(11) DEFAULT NULL,
  `idGroupeTravail` int(11) DEFAULT NULL,
  KEY `idUtilisateur` (`idUtilisateur`),
  KEY `idGroupeTravail` (`idGroupeTravail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `estdansgroupetravail`
--

INSERT INTO `estdansgroupetravail` (`idUtilisateur`, `idGroupeTravail`) VALUES
(1, 1),
(2, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1);

-- --------------------------------------------------------

--
-- Structure de la table `estdansoption`
--

DROP TABLE IF EXISTS `estdansoption`;
CREATE TABLE IF NOT EXISTS `estdansoption` (
  `idOption` int(11) DEFAULT NULL,
  `idUtilisateur` int(11) DEFAULT NULL,
  KEY `idOption` (`idOption`),
  KEY `idUtilisateur` (`idUtilisateur`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `estdansoption`
--

INSERT INTO `estdansoption` (`idOption`, `idUtilisateur`) VALUES
(1, 1),
(1, 2),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(2, 11);

-- --------------------------------------------------------

--
-- Structure de la table `estdansue`
--

DROP TABLE IF EXISTS `estdansue`;
CREATE TABLE IF NOT EXISTS `estdansue` (
  `idUe` int(11) DEFAULT NULL,
  `idUtilisateur` int(11) DEFAULT NULL,
  KEY `idUe` (`idUe`),
  KEY `idUtilisateur` (`idUtilisateur`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `estdansue`
--

INSERT INTO `estdansue` (`idUe`, `idUtilisateur`) VALUES
(1, 1),
(1, 2),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(2, 1),
(2, 2),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(2, 10);

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
CREATE TABLE IF NOT EXISTS `groupe` (
  `idGroupe` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  `typeDeGroupe` int(11) DEFAULT NULL,
  PRIMARY KEY (`idGroupe`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`idGroupe`, `description`, `typeDeGroupe`) VALUES
(1, 'Division 1 LD', 24),
(2, 'Division 2 LD', 24),
(3, 'Equipe Callisto', 8),
(4, 'Division 2 DSMT', 12);

-- --------------------------------------------------------

--
-- Structure de la table `groupeaudit`
--

DROP TABLE IF EXISTS `groupeaudit`;
CREATE TABLE IF NOT EXISTS `groupeaudit` (
  `idGroupeAudit` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idGroupeAudit`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `groupeaudit`
--

INSERT INTO `groupeaudit` (`idGroupeAudit`, `description`) VALUES
(1, 'Ceci est la description du groupe d\'audit');

-- --------------------------------------------------------

--
-- Structure de la table `groupetravail`
--

DROP TABLE IF EXISTS `groupetravail`;
CREATE TABLE IF NOT EXISTS `groupetravail` (
  `idGroupeTravail` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idGroupeTravail`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `groupetravail`
--

INSERT INTO `groupetravail` (`idGroupeTravail`, `description`) VALUES
(1, 'Equipe Callisto');

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `listeeleve`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `listeeleve`;
CREATE TABLE IF NOT EXISTS `listeeleve` (
`idUtilisateur` int(11)
,`prenom` varchar(20)
,`nom` varchar(20)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `listegroupeaudit`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `listegroupeaudit`;
CREATE TABLE IF NOT EXISTS `listegroupeaudit` (
`nom` varchar(20)
,`prenom` varchar(20)
,`idGroupeAudit` int(11)
,`description` varchar(100)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `listegroupetravail`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `listegroupetravail`;
CREATE TABLE IF NOT EXISTS `listegroupetravail` (
`nom` varchar(20)
,`prenom` varchar(20)
,`idGroupeTravail` int(11)
,`description` varchar(250)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `listeinterne`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `listeinterne`;
CREATE TABLE IF NOT EXISTS `listeinterne` (
`nom` varchar(20)
,`prenom` varchar(20)
,`nomMatiere` varchar(100)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `listequestion`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `listequestion`;
CREATE TABLE IF NOT EXISTS `listequestion` (
`id_audit` int(11)
,`question` varchar(250)
,`type` enum('texte','checkbox')
,`reponse` varchar(250)
,`nom_audit` varchar(40)
,`id_groupe_audit` int(11)
,`nom` varchar(20)
,`prenom` varchar(20)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `listereponsableoption`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `listereponsableoption`;
CREATE TABLE IF NOT EXISTS `listereponsableoption` (
`prenom` varchar(20)
,`nom` varchar(20)
,`nomOption` varchar(100)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `listeresponsablematiere`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `listeresponsablematiere`;
CREATE TABLE IF NOT EXISTS `listeresponsablematiere` (
`nom` varchar(20)
,`prenom` varchar(20)
,`nomMatiere` varchar(100)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `listeresponsableue`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `listeresponsableue`;
CREATE TABLE IF NOT EXISTS `listeresponsableue` (
`nom` varchar(20)
,`prenom` varchar(20)
,`nomUe` varchar(100)
);

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

DROP TABLE IF EXISTS `matiere`;
CREATE TABLE IF NOT EXISTS `matiere` (
  `idMatiere` int(11) NOT NULL AUTO_INCREMENT,
  `nomMatiere` varchar(100) DEFAULT NULL,
  `responsableMatiere` int(11) DEFAULT NULL,
  PRIMARY KEY (`idMatiere`),
  KEY `responsableMatiere` (`responsableMatiere`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `matiere`
--

INSERT INTO `matiere` (`idMatiere`, `nomMatiere`, `responsableMatiere`) VALUES
(1, 'Sprint 1', 1),
(2, 'Bases de données relationnelles avancee', 3),
(3, 'Bases de données relationnelles simple', 3),
(4, 'Test', 12);

-- --------------------------------------------------------

--
-- Structure de la table `matieredansue`
--

DROP TABLE IF EXISTS `matieredansue`;
CREATE TABLE IF NOT EXISTS `matieredansue` (
  `idMatiere` int(11) DEFAULT NULL,
  `idUe` int(11) DEFAULT NULL,
  KEY `idMatiere` (`idMatiere`),
  KEY `idUe` (`idUe`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `matieredansue`
--

INSERT INTO `matieredansue` (`idMatiere`, `idUe`) VALUES
(1, 2),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `modeleaudit`
--

DROP TABLE IF EXISTS `modeleaudit`;
CREATE TABLE IF NOT EXISTS `modeleaudit` (
  `idModeleAudit` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idModeleAudit`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `modeleaudit`
--

INSERT INTO `modeleaudit` (`idModeleAudit`, `description`) VALUES
(1, 'Ceci est un type d\'audit');

-- --------------------------------------------------------

--
-- Structure de la table `option`
--

DROP TABLE IF EXISTS `option`;
CREATE TABLE IF NOT EXISTS `option` (
  `idOption` int(11) NOT NULL AUTO_INCREMENT,
  `nomOption` varchar(100) DEFAULT NULL,
  `responsableOption` int(11) DEFAULT NULL,
  PRIMARY KEY (`idOption`),
  KEY `responsableOption` (`responsableOption`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `option`
--

INSERT INTO `option` (`idOption`, `nomOption`, `responsableOption`) VALUES
(1, 'Logiciel & Donnees', 3),
(2, 'DSMT', 4);

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `idQuestion` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(250) DEFAULT NULL,
  `type` enum('texte','checkbox') DEFAULT NULL,
  PRIMARY KEY (`idQuestion`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `question`
--

INSERT INTO `question` (`idQuestion`, `question`, `type`) VALUES
(1, 'etes vous satisfait?', 'checkbox'),
(2, 'quest ce que vous avez fait?', 'texte');

-- --------------------------------------------------------

--
-- Structure de la table `reponse`
--

DROP TABLE IF EXISTS `reponse`;
CREATE TABLE IF NOT EXISTS `reponse` (
  `idReponse` int(11) NOT NULL AUTO_INCREMENT,
  `idQuestion` int(11) DEFAULT NULL,
  `reponse` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idReponse`),
  KEY `idQuestion` (`idQuestion`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `reponse`
--

INSERT INTO `reponse` (`idReponse`, `idQuestion`, `reponse`) VALUES
(1, 1, 'Ceci est la réponse à la question');

-- --------------------------------------------------------

--
-- Structure de la table `responsable`
--

DROP TABLE IF EXISTS `responsable`;
CREATE TABLE IF NOT EXISTS `responsable` (
  `idResponsable` int(11) NOT NULL AUTO_INCREMENT,
  `utilisateurId` int(11) DEFAULT NULL,
  PRIMARY KEY (`idResponsable`),
  KEY `utilisateurId` (`utilisateurId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `responsable`
--

INSERT INTO `responsable` (`idResponsable`, `utilisateurId`) VALUES
(1, 3),
(2, 12),
(3, 4);

-- --------------------------------------------------------

--
-- Structure de la table `ue`
--

DROP TABLE IF EXISTS `ue`;
CREATE TABLE IF NOT EXISTS `ue` (
  `idUe` int(11) NOT NULL AUTO_INCREMENT,
  `nomUe` varchar(100) DEFAULT NULL,
  `responsableUe` int(11) DEFAULT NULL,
  PRIMARY KEY (`idUe`),
  KEY `responsableUe` (`responsableUe`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ue`
--

INSERT INTO `ue` (`idUe`, `nomUe`, `responsableUe`) VALUES
(1, 'Bases de données relationnelles', 4),
(2, 'Projet LD', 4);

-- --------------------------------------------------------

--
-- Structure de la table `uedansoption`
--

DROP TABLE IF EXISTS `uedansoption`;
CREATE TABLE IF NOT EXISTS `uedansoption` (
  `idUe` int(11) DEFAULT NULL,
  `idOption` int(11) DEFAULT NULL,
  KEY `idUe` (`idUe`),
  KEY `idOption` (`idOption`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `uedansoption`
--

INSERT INTO `uedansoption` (`idUe`, `idOption`) VALUES
(1, 1),
(2, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) DEFAULT NULL,
  `prenom` varchar(20) DEFAULT NULL,
  `adresseMail` varchar(100) DEFAULT NULL,
  `motDePasse` varchar(100) DEFAULT NULL,
  `login` varchar(100) DEFAULT NULL,
  `eleve` int(1) DEFAULT NULL,
  PRIMARY KEY (`idUtilisateur`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idUtilisateur`, `nom`, `prenom`, `adresseMail`, `motDePasse`, `login`, `eleve`) VALUES
(1, 'Mignon', 'Stephane', 'stephane.mignon@reseau.eseo.fr', 'mdp1', 'login', 1),
(2, 'Godalier', 'Gilian', 'gilian.godalier@reseau.eseo.fr', 'mdp2', 'login', 1),
(3, 'Rousseau', 'Sophie', 'sophie.rousseau@reseau.eseo.fr', 'mdp3', 'login', 0),
(4, 'Beaudoux', 'Olivier', 'olivier.baudoux@reseau.eseo.fr', 'mdp4', 'login', 0),
(5, 'Gbeassor', 'Harry', 'harry.gbeassor@reseau.eseo.fr', 'mdp5', 'login', 1),
(6, 'Nouari', 'Zeinab', 'zeinab.nouari@reseau.eseo.fr', 'mdp6', 'login', 1),
(7, 'Jouaud', 'Armand', 'armand.jouaud@reseau.eseo.fr', 'mdp7', 'login', 1),
(8, 'Danion', 'Mathis', 'mathis.danion@reseau.eseo.fr', 'mdp8', 'login', 1),
(9, 'Chamard', 'Thomas', 'thomas.chamard@reseau.eseo.fr', 'mdp9', 'login', 1),
(10, 'Naud', 'Pierre', 'pierre.naud@reseau.eseo.fr', 'mdp1', 'login1', 1),
(11, 'Riggi', 'Thibault', 'thibault.riggi@reseau.eseo.fr', 'mdp2', 'login1', 1),
(12, 'Barton', 'Steve', 'barton.steve@gmail.com', 'hello', 'stevebarton', 0);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `vueaudit`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `vueaudit`;
CREATE TABLE IF NOT EXISTS `vueaudit` (
`idAudit` int(11)
,`nomEncadrant` varchar(20)
,`prenomEncadrant` varchar(20)
,`idRespMatiere` int(11)
,`nomRespMatiere` varchar(20)
,`prenomRespMatiere` varchar(20)
,`idEleve` int(11)
,`nomEleve` varchar(20)
,`prenomEleve` varchar(20)
,`idGroupe` int(11)
,`UE` int(11)
,`OptionEleve` int(11)
);

-- --------------------------------------------------------

--
-- Structure de la vue `listeeleve`
--
DROP TABLE IF EXISTS `listeeleve`;

DROP VIEW IF EXISTS `listeeleve`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listeeleve`  AS  select `utilisateur`.`idUtilisateur` AS `idUtilisateur`,`utilisateur`.`prenom` AS `prenom`,`utilisateur`.`nom` AS `nom` from `utilisateur` where `utilisateur`.`eleve` = 1 order by `utilisateur`.`idUtilisateur` ;

-- --------------------------------------------------------

--
-- Structure de la vue `listegroupeaudit`
--
DROP TABLE IF EXISTS `listegroupeaudit`;

DROP VIEW IF EXISTS `listegroupeaudit`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listegroupeaudit`  AS  select `utilisateur`.`nom` AS `nom`,`utilisateur`.`prenom` AS `prenom`,`groupeaudit`.`idGroupeAudit` AS `idGroupeAudit`,`groupeaudit`.`description` AS `description` from ((`utilisateur` join `groupeaudit`) join `estdansgroupeaudit`) where `utilisateur`.`idUtilisateur` = `estdansgroupeaudit`.`idUtilisateur` and `groupeaudit`.`idGroupeAudit` = `estdansgroupeaudit`.`idGroupeAudit` order by `groupeaudit`.`idGroupeAudit` ;

-- --------------------------------------------------------

--
-- Structure de la vue `listegroupetravail`
--
DROP TABLE IF EXISTS `listegroupetravail`;

DROP VIEW IF EXISTS `listegroupetravail`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listegroupetravail`  AS  select `utilisateur`.`nom` AS `nom`,`utilisateur`.`prenom` AS `prenom`,`groupetravail`.`idGroupeTravail` AS `idGroupeTravail`,`groupetravail`.`description` AS `description` from ((`utilisateur` join `groupetravail`) join `estdansgroupetravail`) where `utilisateur`.`idUtilisateur` = `estdansgroupetravail`.`idUtilisateur` and `estdansgroupetravail`.`idGroupeTravail` = `groupetravail`.`idGroupeTravail` order by `groupetravail`.`idGroupeTravail` ;

-- --------------------------------------------------------

--
-- Structure de la vue `listeinterne`
--
DROP TABLE IF EXISTS `listeinterne`;

DROP VIEW IF EXISTS `listeinterne`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listeinterne`  AS  select `utilisateur`.`nom` AS `nom`,`utilisateur`.`prenom` AS `prenom`,`matiere`.`nomMatiere` AS `nomMatiere` from ((`utilisateur` join `matiere`) join `encadre`) where `encadre`.`idMatiere` = `matiere`.`idMatiere` and `encadre`.`idUtilisateur` = `utilisateur`.`idUtilisateur` order by `matiere`.`idMatiere` ;

-- --------------------------------------------------------

--
-- Structure de la vue `listequestion`
--
DROP TABLE IF EXISTS `listequestion`;

DROP VIEW IF EXISTS `listequestion`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listequestion`  AS  select `audit`.`idAudit` AS `id_audit`,`question`.`question` AS `question`,`question`.`type` AS `type`,`reponse`.`reponse` AS `reponse`,`audit`.`nom` AS `nom_audit`,`groupeaudit`.`idGroupeAudit` AS `id_groupe_audit`,`utilisateur`.`nom` AS `nom`,`utilisateur`.`prenom` AS `prenom` from ((((((`question` join `reponse`) join `audit`) join `auditesur`) join `utilisateur`) join `groupeaudit`) join `estdansgroupeaudit`) where `question`.`idQuestion` = `reponse`.`idQuestion` and `auditesur`.`idAudit` = `audit`.`idAudit` and `auditesur`.`idReponse` = `reponse`.`idReponse` and `auditesur`.`idGroupeAudit` = `groupeaudit`.`idGroupeAudit` and `estdansgroupeaudit`.`idGroupeAudit` = `groupeaudit`.`idGroupeAudit` and `estdansgroupeaudit`.`idUtilisateur` = `utilisateur`.`idUtilisateur` order by `audit`.`idAudit` ;

-- --------------------------------------------------------

--
-- Structure de la vue `listereponsableoption`
--
DROP TABLE IF EXISTS `listereponsableoption`;

DROP VIEW IF EXISTS `listereponsableoption`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listereponsableoption`  AS  select `utilisateur`.`prenom` AS `prenom`,`utilisateur`.`nom` AS `nom`,`option`.`nomOption` AS `nomOption` from (`option` join `utilisateur`) where `option`.`responsableOption` = `utilisateur`.`idUtilisateur` order by `option`.`idOption` ;

-- --------------------------------------------------------

--
-- Structure de la vue `listeresponsablematiere`
--
DROP TABLE IF EXISTS `listeresponsablematiere`;

DROP VIEW IF EXISTS `listeresponsablematiere`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listeresponsablematiere`  AS  select `utilisateur`.`nom` AS `nom`,`utilisateur`.`prenom` AS `prenom`,`matiere`.`nomMatiere` AS `nomMatiere` from (`matiere` join `utilisateur`) where `matiere`.`responsableMatiere` = `utilisateur`.`idUtilisateur` order by `matiere`.`idMatiere` ;

-- --------------------------------------------------------

--
-- Structure de la vue `listeresponsableue`
--
DROP TABLE IF EXISTS `listeresponsableue`;

DROP VIEW IF EXISTS `listeresponsableue`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listeresponsableue`  AS  select `utilisateur`.`nom` AS `nom`,`utilisateur`.`prenom` AS `prenom`,`ue`.`nomUe` AS `nomUe` from (`ue` join `utilisateur`) where `ue`.`responsableUe` = `utilisateur`.`idUtilisateur` order by `ue`.`idUe` ;

-- --------------------------------------------------------

--
-- Structure de la vue `vueaudit`
--
DROP TABLE IF EXISTS `vueaudit`;

DROP VIEW IF EXISTS `vueaudit`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vueaudit`  AS  select `audit`.`idAudit` AS `idAudit`,`enc`.`nom` AS `nomEncadrant`,`enc`.`prenom` AS `prenomEncadrant`,`respmatiere`.`idUtilisateur` AS `idRespMatiere`,`respmatiere`.`nom` AS `nomRespMatiere`,`respmatiere`.`prenom` AS `prenomRespMatiere`,`eleve`.`idUtilisateur` AS `idEleve`,`eleve`.`nom` AS `nomEleve`,`eleve`.`prenom` AS `prenomEleve`,`groupe`.`idGroupe` AS `idGroupe`,`ue`.`idUe` AS `UE`,`edo`.`idOption` AS `OptionEleve` from ((((((((((`audit` join `utilisateur` `enc`) join `encadrant`) join `matiere`) join `utilisateur` `respmatiere`) join `utilisateur` `eleve`) join `contient_eleve_audit` `auditeleve`) join `groupe`) join `contient_ue_matiere` `matue`) join `ue`) join `estdansoption` `edo`) where `audit`.`idEncadrant` = `encadrant`.`idEncadrant` and `encadrant`.`utilisateurId` = `enc`.`idUtilisateur` and `audit`.`idMatiere` = `matiere`.`idMatiere` and `matiere`.`responsableMatiere` = `respmatiere`.`idUtilisateur` and `audit`.`idAudit` = `auditeleve`.`idAudit` and `auditeleve`.`idEleve` = `eleve`.`idUtilisateur` and `auditeleve`.`idGroupe` = `groupe`.`idGroupe` and `matiere`.`idMatiere` = `matue`.`matiere` and `matue`.`ue` = `ue`.`idUe` and `eleve`.`idUtilisateur` = `edo`.`idUtilisateur` ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
