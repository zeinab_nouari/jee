package fr.eseo.ld.callisto.projetgl;

import java.io.File;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.junit.AfterClass;
import org.junit.Assert;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;


import fr.eseo.ld.callisto.projetgl.selenium.configuration.Configuration;

/**
 * La classe abstraite 'AuditTest' est la classe parent utilis�e par les classes filles supportant les diff�rents
 * navigateurs webs. Contient diff�rentes m�thodes. Inspir�e de la classe 'EseoTest' de Richard Woodward
 * 
 * @author Pierre Naud
 *
 */

public abstract class AuditTest {

	// Instance de WebDriver (diff�rente en fonction du navigateur)

	protected static  WebDriver driver;

	/**
	 * Method that can create a png file containing a screenshot of the browsers
	 * window. Could be useful to compare and contract the different rendering of
	 * the same website by different web browsers.
	 * 
	 * @param imageFilename the name of the file that will store the png image.
	 */
	protected void createScreenshot(String imageFilename) {
		try {
			File screenshot = ((TakesScreenshot) AuditTest.driver).getScreenshotAs(OutputType.FILE);
			ImageIO.write(ImageIO.read(screenshot), "PNG", new File(imageFilename));
		} catch (IOException ioe) {
			Assert.fail("Could not save screenshot: " + ioe.getMessage());
		}
	}

	/**
	 * Method that asks the WebDriver to open a webpage in the chosen web browser.
	 * 
	 * @param screenshotFilename the name of the file that will store the png image.
	 */
	protected void openWebPage(String screenshotFilename) {
		AuditTest.driver.get(Configuration.getString("eseo_address"));
		Assert.assertEquals("Web page title does not appear correct.", Configuration.getString("eseo_homepage_title"),
				AuditTest.driver.getTitle());
		createScreenshot(screenshotFilename);
	}

	/**
	 * The teardown method is used to quit the WebDriver, that is to say, close the
	 * web browser and free up system resources.
	 */
	@AfterClass
	public static void teardown() {
		driver.quit();
	}

	/**
	 * Verifies that a file exists in the driverlocation.
	 * 
	 * @param driverLocation the absolute path to the webdriver file.
	 */
	public static void verifyDriverLocation(String driverLocation) {
		File driverFile = new File(driverLocation);
		if (!driverFile.exists()) {
			Assert.fail("Could not find executable: " + driverLocation);
		}
	}

	/**
	 * Setup the WebDriver instance to use certain properties. An implicit timeout
	 * of two seconds and a window size of 1024 x 768 pixels.
	 * 
	 */
	public static void setup() {

		AuditTest.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		AuditTest.driver.manage().window().setSize(new Dimension(1024, 768));

	}


}
