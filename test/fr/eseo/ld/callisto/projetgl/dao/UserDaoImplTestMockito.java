package fr.eseo.ld.callisto.projetgl.dao;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import fr.eseo.ld.callisto.projetgl.beans.Utilisateur;




/**
 * Classe de test unitaire de la classe 'UserDaoImpl'
 * 
 * @author Pierre Naud
 *
 */

@RunWith(MockitoJUnitRunner.class)
public class UserDaoImplTestMockito {

	@Mock
	private Utilisateur mockUtilisateur;
	
	@Mock
	private DataSource mockDataSource;
	
	@Mock
	private Connection mockConnection;
	
	@Mock
	private PreparedStatement mockPreparedStatement;

	@Mock
	private ResultSet mockResultSet;
	
	@BeforeClass
	public static void setUpClass() throws Exception{
		System.out.println("Test Mockito beginning");
	}

	@AfterClass
	public static void tearDownClass() {
		System.out.println("Test Mockito ending");
	}
	
	/**
	 * Test "estEleve"
	 * @throws DaoException 
	 * @throws SQLException 
	 */

	@Test
	public void testEstEleveTrue() throws DaoException, SQLException {
		final UserDaoImpl userDaoImpl = new UserDaoImpl(mockConnection);
		
		when(mockConnection.prepareStatement(Mockito.anyString())).thenReturn(mockPreparedStatement);
		doNothing().when(mockPreparedStatement).setInt(Mockito.anyInt(), Mockito.anyInt());
		when(mockPreparedStatement.executeQuery()).thenReturn(mockResultSet);
		when(mockResultSet.next()).thenReturn(Boolean.TRUE);
		when(mockResultSet.getBoolean("eleve")).thenReturn(true);
		assertEquals("L'utilisateur est un �l�ve", true, userDaoImpl.estEleve(this.mockUtilisateur.getUserId()));
	}
	
	@Test
	public void testEstEleveFalse() throws SQLException, DaoException {
		final UserDaoImpl userDaoImpl = new UserDaoImpl(mockConnection);
	
		when(mockConnection.prepareStatement(Mockito.anyString())).thenReturn(mockPreparedStatement);
		doNothing().when(mockPreparedStatement).setInt(Mockito.anyInt(), Mockito.anyInt());
		when(mockPreparedStatement.executeQuery()).thenReturn(mockResultSet);
		when(mockResultSet.next()).thenReturn(Boolean.TRUE);
		when(mockResultSet.getBoolean("eleve")).thenReturn(false);
		assertEquals("L'utilisateur est un �l�ve", false, userDaoImpl.estEleve(this.mockUtilisateur.getUserId()));
	}
	
	@Test (expected = DaoException.class)
	public void whenNoEleveThrowDaoException() throws SQLException, DaoException {
		final UserDaoImpl userDaoImpl = new UserDaoImpl(mockConnection);
		
		when(mockConnection.prepareStatement(Mockito.anyString())).thenReturn(mockPreparedStatement);
		doNothing().when(mockPreparedStatement).setInt(Mockito.anyInt(), Mockito.anyInt());
		when(mockPreparedStatement.executeQuery()).thenReturn(mockResultSet);
		when(mockResultSet.next()).thenReturn(Boolean.FALSE);
		when(userDaoImpl.estEleve(mockUtilisateur.getUserId())).thenThrow(new DaoException("Utilisateur introuvable"));
		verify(userDaoImpl, times(1)).estEleve(mockUtilisateur.getUserId());
	}
	

}
