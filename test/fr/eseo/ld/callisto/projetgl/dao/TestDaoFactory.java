package fr.eseo.ld.callisto.projetgl.dao;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

public class TestDaoFactory {
	@Test
	public void testConnexion() throws DaoException {
		DaoFactory daoFact = null;
		try {
			daoFact = DaoFactory.getInstance();
			assertTrue("La connexion n'est pas établie", daoFact.getConnection()!=null);
		} catch (SQLException e) {
			fail("La connexion n'est pas établie "+e.getMessage());
		}
	}
}
