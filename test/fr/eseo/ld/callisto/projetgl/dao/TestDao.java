package fr.eseo.ld.callisto.projetgl.dao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestDaoFactory.class, TestUserDaoImpl.class })
public class TestDao {

}
