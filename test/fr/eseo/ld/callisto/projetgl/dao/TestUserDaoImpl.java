package fr.eseo.ld.callisto.projetgl.dao;

import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import fr.eseo.ld.callisto.projetgl.beans.Audit;
import fr.eseo.ld.callisto.projetgl.beans.BeanException;
import fr.eseo.ld.callisto.projetgl.beans.Encadrant;
import fr.eseo.ld.callisto.projetgl.beans.Responsable;
import fr.eseo.ld.callisto.projetgl.beans.Utilisateur;



public class TestUserDaoImpl {
	@Test
	public void testRecupererListeUtilisateur() {
		DaoFactory daoFact = null;
		try {
			daoFact = DaoFactory.getInstance();
			daoFact.getConnection();
			UtilisateurDaoImpl userDao = new UtilisateurDaoImpl(daoFact.getConnection());
			List<Utilisateur> listeUtilisateur = new ArrayList<Utilisateur>();
			
			listeUtilisateur = userDao.getListUtilisateurs();
			assertTrue("L'utilisateur n'est pas correct",!listeUtilisateur.isEmpty());
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRecupererUtilisateur() {
		DaoFactory daoFact = null;
		try {
			daoFact = DaoFactory.getInstance();
			daoFact.getConnection();
			UtilisateurDaoImpl userDao = new UtilisateurDaoImpl(daoFact.getConnection());
			Utilisateur user= userDao.getUtilisateur("Rousseau","Sophie");
			assertTrue("L'utilisateur n'est pas correct",user!=null);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testEstResponsable() {
		DaoFactory daoFact = null;
		try {
			daoFact = DaoFactory.getInstance();
			daoFact.getConnection();
			UtilisateurDaoImpl userDao = new UtilisateurDaoImpl(daoFact.getConnection());
			Utilisateur user= userDao.getUtilisateur("Rousseau","Sophie");
			boolean estResponsable=userDao.estResponsable(user);
			assertTrue("L'utilisateur n'est pas correct",estResponsable==true);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testgetResponsable() throws BeanException {
		DaoFactory daoFact = null;
		try {
			daoFact = DaoFactory.getInstance();
			daoFact.getConnection();
			UtilisateurDaoImpl userDao = new UtilisateurDaoImpl(daoFact.getConnection());
			Utilisateur user= userDao.getUtilisateur("Rousseau","Sophie");
			Responsable responsable = userDao.getResponsable(user);
			assertTrue("L'utilisateur n'est pas correct",responsable!=null);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testEstEncadrant() {
		DaoFactory daoFact = null;
		try {
			daoFact = DaoFactory.getInstance();
			daoFact.getConnection();
			UtilisateurDaoImpl userDao = new UtilisateurDaoImpl(daoFact.getConnection());
			Utilisateur user= userDao.getUtilisateur("Rousseau","Sophie");
			boolean estResponsable=userDao.estEncadrant(user);
			assertTrue("L'utilisateur n'est pas correct",estResponsable==true);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testgetEncadrant() throws BeanException {
		DaoFactory daoFact = null;
		try {
			daoFact = DaoFactory.getInstance();
			daoFact.getConnection();
			UtilisateurDaoImpl userDao = new UtilisateurDaoImpl(daoFact.getConnection());
			Utilisateur user= userDao.getUtilisateur("Rousseau","Sophie");
			Encadrant encadrant = userDao.getEncadrant(user);
			assertTrue("L'utilisateur n'est pas correct",encadrant!=null);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testgetListeAudit() throws BeanException {
		DaoFactory daoFact = null;
		try {
			daoFact = DaoFactory.getInstance();
			daoFact.getConnection();
			UtilisateurDaoImpl userDao = new UtilisateurDaoImpl(daoFact.getConnection());
			Utilisateur user= userDao.getUtilisateur("Rousseau","Sophie");
			Encadrant encadrant = userDao.getEncadrant(user);
			List<Audit> listeAudit = userDao.getListeAuditsByEncadrant(encadrant);
			System.out.println(listeAudit);
			assertTrue("L'utilisateur n'est pas correct",!listeAudit.isEmpty());
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}
}
