package fr.eseo.ld.callisto.projetgl.servlets;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import fr.eseo.ld.callisto.projetgl.AuditTest;
import fr.eseo.ld.callisto.projetgl.selenium.configuration.Configuration;

/**
 * Classe fonctionnant pour Firefox
 * 
 * @author Pierre Naud
 *
 */

public class RolesUtilisateursTestFirefox extends AuditTest{
	private static final String SYSTEM_PROPERTY = Configuration.getString("firefox_driver");

	private static final String DRIVER_LOCATION = Configuration.getString("firefox_path");

	@BeforeClass
	public static void setup() {
		AuditTest.verifyDriverLocation(DRIVER_LOCATION);
		System.setProperty(SYSTEM_PROPERTY,DRIVER_LOCATION);
		AuditTest.driver = new FirefoxDriver();
		AuditTest.setup();

	}
	
	/**
	 * M�thode qui teste l'ouverture de la page web
	 */
	
	protected void openWebPage(String screenshot) {
		AuditTest.driver.get(Configuration.getString("changement_roles_addr"));
		Assert.assertEquals("Web page title does not appear correct.", Configuration.getString("changement_role_title"),
				AuditTest.driver.getTitle());
		createScreenshot(screenshot);
	}
	
	/**
	 * M�thode qui vient tester le changement de r�les (un �l�ve ne peut pas devenir encadrant); 
	 */
	
	protected void modifRole() {
		AuditTest.driver.get(Configuration.getString("changement_roles_addr"));
		Select listUser = new Select(AuditTest.driver.findElement(By.id("listeUtilisateurs")));
		listUser.selectByVisibleText("Danion Mathis");	
		Select role = new Select(AuditTest.driver.findElement(By.id("roles")));
		role.selectByVisibleText("Encadrant mati�re");
		Select listeUe = new Select(AuditTest.driver.findElement(By.id("listeUe")));
		listeUe.selectByVisibleText("Test");	
		Select listeOptions = new Select(AuditTest.driver.findElement(By.id("listeOptions")));
		listeOptions.selectByVisibleText("LD");
		Select listeMatieres = new Select(AuditTest.driver.findElement(By.id("listeMatieres")));
		listeMatieres.selectByVisibleText("Conception bdd");
		AuditTest.driver.findElement(By.id("formRole")).submit();
		
		WebDriverWait wait = new WebDriverWait(AuditTest.driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("formRole")));
		
		
	}

	@Test
	public void testOpenWebPageFirefox() {
		this.openWebPage("screenshot_firefox.png");
		this.modifRole();
	}

}